#-----------------------------------
# Imports
#-----------------------------------

from typing import List
from .plci_core_diagram import Diagram
from .plci_core_box import Box
from .plci_core_signal_event import SignalEvent

#-----------------------------------
# Class
#-----------------------------------

class SchedulerException :
    """Est la classe qui permet de créer un retour lors d'une exception dans un planificateur."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self, box_ : Box, box_bis_ : Box, events_ : List[SignalEvent], event_ : SignalEvent, diagram_ : Diagram, timing_ : float, exception_ : Exception) :
        self._box = box_
        self._box_bis = box_bis_
        self._events = events_
        self._event = event_
        self._diagram = diagram_
        self._exception = exception_
        self._timing = timing_

    
    # -------------
    # Methods
    # -------------

    def print (self) :
        print('')
        print("===================")
        print("scheduler exception")
        print("===================")
        print('')

        print("Eexception at : ", self._timing, " s")

        if self._box is None :
            print("Box : No current box")
        else :
            print("Box : ", self._box.library, self._box.name, " | index : ", self._box.index)

        if self._box_bis is None :
            print("Bis box : No current bis box")
        else :
            print("Bis box : ", self._box.library, self._box.name, " | index : ", self._box_bis.index)

        if self._event is None :
            print("Current event : No current event")
        else :
            print("Current event : box index : ", self._event.box.index, " | signal index : ", self._event.signal.index)

            if not self._event.signal in self._diagram.box_inputs :
                print("The signal does not have any box to tickle !!!")

        if self._events is None :
            print("Events list : No events list")
        else :
            print("Events list : ", len(self._events), " events in the queue")

        print(" -- ")
        print(self._exception)
        print(" -- ")