#-----------------------------------
# Imports
#-----------------------------------

from typing import Any

from .plci_core_data import Data
from .plci_core_data_type import DataType

#-----------------------------------
# Class
#-----------------------------------

class Signal :
    """Est la classe qui gère les différents signaux du diagramme."""

    # -------------
    # Constructors
    # -------------
    def __init__ (self, index_ : int, signal_type_ : DataType, default_value_ : Any) :

        self._index = index_
        self._signal_type = signal_type_
        self._data = Data(self._signal_type, default_value_)

    # -------------
    # Properties
    # -------------
    
    @property
    def index (self) -> int :
        """Permet de récuperer l'index."""    
        return self._index

    @property
    def signal_type (self) -> DataType :
        """Permet de récuperer le type de signal."""
        return self._signal_type
    
    @property
    def data (self) -> Data :
        """Permet de récuperer la donnée actuelle du signal."""
        return self._data

    @data.setter
    def data (self, data_ : Data) -> None : 
        """Permet de modifier la donnée interne du signal."""

        if (self._data.data_type == data_.data_type)  :
            self._data = data_
            return

    @property 
    def value (self) -> Any :
        """Permet de récuperer la valeur actuelle du signal."""
        return self._data.value
        
    def __str__ (self) -> str :
        """Permet de retourner un string décrivant le signal."""
        return "Signal [ " + str(self.index) + " -> " + str(self._signal_type) + "]"