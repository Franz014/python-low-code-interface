#-----------------------------------
# Imports
#-----------------------------------
 
from typing import List, Dict

from packages.core.box_library.plci_core_boxes_libraries import BoxesLibraries
from packages.core.plci_core_box import Box
from packages.core.plci_core_diagram import Diagram
from packages.core.plci_core_scheduler import Scheduler
from packages.core.plci_core_scheduler_params import SchedulerParams
from packages.core.plci_core_signal import Signal
from packages.core.scheduler_library.plci_core_schedulers_libraries import SchedulersLibraries
from packages.interface.models.editable_models.scheduler_model import SchedulerModel
from packages.interface.models.editable_models.simulation_model import SimulationModel
from packages.interface.models.graphical_models.box_model import BoxModel
from packages.interface.models.signals_model.abstract_signal_model import AbstractSignalModel

#-----------------------------------
# Class
#-----------------------------------

class Parser : 

    # -------------
    # Constructors
    # -------------

    def __init__ (self) :
        ...
    
    # -------------
    # Methods
    # -------------      

    def parse (self, element : object, boxes_libraries : BoxesLibraries, schedulers_libraries : SchedulersLibraries) -> Scheduler :
        """Est la méthode pour convertir/traduire un objet en un scheduler prêt à être simuler."""
        
        if isinstance(element, SimulationModel) :
            
            return self.__parse_simulation__(element, boxes_libraries, schedulers_libraries)

        elif isinstance(element, str) :
            ...

        return None

    def __parse_simulation__ (self, simulation_model : SimulationModel, boxes_libraries : BoxesLibraries, schedulers_libraries : SchedulersLibraries) -> Scheduler :
        """Est la méthode pour convertir/traduire une simulation en un scheduler prêt à être simuler."""

        if simulation_model.scheduler_model is None :
            return None

        else :
            if simulation_model.scheduler_model.params is None :
                return None 

            else :

                scheduler = schedulers_libraries.construct_scheduler(simulation_model.scheduler_model.library, simulation_model.scheduler_model.name)

                # Si pas de scheduler sélectionné.
                if scheduler is None : 
                    
                    scheduler_library = None 

                    # On prend le premier.
                    for lib in schedulers_libraries.schedulers :
                        if not(lib is None) :
                            scheduler_library = lib
                            break

                    if scheduler_library is None :
                        return None

                    else :
                        simulation_model.scheduler_model = SchedulerModel(scheduler_library.scheduler_library, scheduler_library.scheduler_name, simulation_model.scheduler_model.params)
                        scheduler = scheduler_library.construct_scheduler()

                scheduler.params = SchedulerParams(simulation_model.scheduler_model.params.stop_time, simulation_model.scheduler_model.params.step_time)
                scheduler.diagram = Diagram()
                
                last_signal_index = 0
                signals_dict : Dict[AbstractSignalModel, Signal] = {} 
                diagrams : List[Diagram] = []

                for box_model in simulation_model.box :

                    if isinstance(box_model, BoxModel) :

                        params = {}
                        for param in box_model.get_parameters() :
                            params[param] = box_model.get_parameter(param)['value']

                        box = boxes_libraries.construct_box(box_model.library, box_model.name, **params)
                        scheduler.diagram.append(box)

                        if isinstance(box, Box) :
                            for input in box_model.inputs :
                                if len(input._links) > 0 :
                                    link = input._links[0]

                                    if link in signals_dict : 
                                        scheduler.diagram.add_box_inputs(box, signals_dict[link])

                                    else : 
                                        signal : Signal = Signal(last_signal_index, link.data_type, link.data_type.default_value())
                                        scheduler.diagram.append(signal)
                                        signals_dict[link] = signal
                                        scheduler.diagram.add_box_inputs(box, signal)
                                        last_signal_index += 1
                                        

                            for output in box_model.outputs : 
                                
                                for link in output.links : 
                                    
                                    if link in signals_dict : 
                                        scheduler.diagram.add_box_outputs(box, signals_dict[link])

                                    else : 
                                        signal : Signal = Signal(last_signal_index, link.data_type, link.data_type.default_value())
                                        scheduler.diagram.append(signal)
                                        signals_dict[link] = signal
                                        scheduler.diagram.add_box_outputs(box, signal)
                                        last_signal_index += 1

                        elif isinstance(box, Diagram) : 
                            diagrams.append(box)
                            

                return scheduler

                        