#-----------------------------------
# Imports
#-----------------------------------

import sys as s
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QLabel, QCheckBox, QSpinBox, QLineEdit
from packages.core.plci_core_data_type import DataType
from packages.interface.graphical_actions.diagram_change_element_params_action import DiagramChangeElementParamsAction
from packages.interface.models.dbl_spin_box import DblSpinBox
from packages.interface.properties_pages.abstract_properties_editor import AbstractPropertiesEditor
from packages.interface.models.graphical_models.box_model import BoxModel


#-----------------------------------
# Class
#-----------------------------------

class ParameterPropertiesEditor (AbstractPropertiesEditor) :
    """Est l'éditeur qui permet modifier un paramètre."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self, box_model : BoxModel, parameter : str) :

        self._parameter = parameter
        self._box_model = box_model
        self._box_model.param_changed.connect(self.box_param_changed)

        AbstractPropertiesEditor.__init__(self, box_model)

        self._lbl_parameter_name_value.setText(QCoreApplication.translate(self.objectName(), self.parameter, None))

        parameter_to_edit = self.box_model.get_parameter(self.parameter)
        self._lbl_parameter_type_value.setText(QCoreApplication.translate(self.objectName(), parameter_to_edit['type'], None))
        self._semaphore_values = True
    
        if parameter_to_edit['type'].__str__().lower() == DataType.FLOAT.__str__().lower() :
            self._widget_value_editor.setValue(parameter_to_edit['value'])

        elif parameter_to_edit['type'].__str__().lower() == DataType.BOOLEAN.__str__().lower() :
            self._widget_value_editor.setChecked(parameter_to_edit['value'])

        elif parameter_to_edit['type'].__str__().lower() == DataType.INT.__str__().lower() :
            self._widget_value_editor.setValue(parameter_to_edit['value'])

        elif parameter_to_edit['type'].__str__().lower() == DataType.STRING.__str__().lower() :
            self._widget_value_editor.setText(parameter_to_edit['value'])

        self._semaphore_values = False
    # -------------
    # Properties
    # -------------

    @property
    def parameter (self) -> str :
        """Permet de récuperer le nom du paramètre qui doit être édité dans la box."""
        return self._parameter

    @property
    def box_model (self) -> BoxModel : 
        """Permet de récuperer le modème de la box dont le paramètre est édité."""
        return self._box_model

    # -------------
    # Methods
    # -------------

    def box_param_changed (self, box, param_name, old_value, new_value) -> None :
        """Est la méthode qui est appelée lorsque la box modifiée par le panel voit un de ses paramètres changer."""

        print('properties editor . box params changed')
        print('- param name :', param_name)
        print('- param value :', new_value)
        print('- param last value :', old_value)

        if not(box == self._model) :
            print('box is not the same')
            return

        if not(param_name == 'params.' + self._parameter) :
            print('param is not the same [params.', self._parameter, ']')
            return
            
        print('box parameter changed.')

        self._semaphore_values = True

        parameter_to_edit = self.box_model.get_parameter(self.parameter)
        if parameter_to_edit['type'].__str__().lower() == DataType.FLOAT.__str__().lower() :
            self._widget_value_editor.setValue(new_value)

        elif parameter_to_edit['type'].__str__().lower() == DataType.BOOLEAN.__str__().lower() :
            self._widget_value_editor.setChecked(new_value)

        elif parameter_to_edit['type'].__str__().lower() == DataType.INT.__str__().lower() :
            self._widget_value_editor.setValue(new_value)

        elif parameter_to_edit['type'].__str__().lower() == DataType.STRING.__str__().lower() :
            self._widget_value_editor.setText(new_value)

        self._semaphore_values = False


    def init_ui (self) -> None :
        
        self._lbl_parameter_name : QLabel = QLabel()
        self._lbl_parameter_name_value : QLabel = QLabel()
        self.layout().addRow(self._lbl_parameter_name, self._lbl_parameter_name_value)

        self._lbl_parameter_type : QLabel = QLabel()
        self._lbl_parameter_type_value : QLabel = QLabel()
        self.layout().addRow(self._lbl_parameter_type, self._lbl_parameter_type_value)

        self._lbl_parameter_value : QLabel = QLabel()
        parameter_to_edit = self.box_model.get_parameter(self.parameter)

        if parameter_to_edit['type'].__str__().lower() == DataType.FLOAT.__str__().lower() :
            self._widget_value_editor : DblSpinBox = DblSpinBox()
            self._widget_value_editor.valueChanged.connect(self.spin_box_value_changed)
            self.layout().addRow(self._lbl_parameter_value, self._widget_value_editor)

        elif parameter_to_edit['type'].__str__().lower() == DataType.BOOLEAN.__str__().lower() :
            self._widget_value_editor : QCheckBox = QCheckBox()
            self._widget_value_editor.stateChanged.connect(self.check_box_value_changed)
            self.layout().addRow(self._lbl_parameter_value, self._widget_value_editor)

        elif parameter_to_edit['type'].__str__().lower() == DataType.INT.__str__().lower() :
            self._widget_value_editor : QSpinBox = QSpinBox()
            self._widget_value_editor.setMaximum(s.maxint)
            self._widget_value_editor.setMinimum(- s.maxint - 1)
            self._widget_value_editor.valueChanged.connect(self.spin_box_value_changed)
            self.layout().addRow(self._lbl_parameter_value, self._widget_value_editor)

        elif parameter_to_edit['type'].__str__().lower() == DataType.STRING.__str__().lower() :
            self._widget_value_editor : QLineEdit = QLineEdit()
            self._widget_value_editor.textEdited.connect(self.text_line_text_edited)
            self.layout().addRow(self._lbl_parameter_value, self._widget_value_editor)

    def translate_ui (self) -> None :
        
        self._lbl_parameter_name.setText(QCoreApplication.translate(self.objectName(), u"Nom : ", None))
        self._lbl_parameter_type.setText(QCoreApplication.translate(self.objectName(), u"Type : ", None))
        self._lbl_parameter_value.setText(QCoreApplication.translate(self.objectName(), u"Valeur : ", None))

    def unload (self) -> None :
        
        self._box_model.param_changed.disconnect(self.box_param_changed)


    def spin_box_value_changed (self) -> None : 
        """Est la méthode appelée lorsque la valeur de la spin box gérant des doubles est modifiée."""

        if self._semaphore_values :
            return

        new_value = self._widget_value_editor.value()
        old_value = self.box_model.get_parameter(self.parameter)['value']
        param_name = 'params.' + self.parameter

        self.actions_generator(old_value, new_value, param_name)
        
        self.box_model.action_param_semaphore = True
        self.box_model.set_parameter(self.parameter, new_value)
        self.box_model.action_param_semaphore = False


    def check_box_value_changed (self) -> None : 
        """Est la méthode appelée lorsque la valeur de la check box est modifiée."""

        if self._semaphore_values :
            return

        print('check box value changed')

        new_value = self._widget_value_editor.isChecked()
        old_value = self.box_model.get_parameter(self.parameter)['value']
        param_name = 'params.' + self.parameter

        action = DiagramChangeElementParamsAction(self.model, param_name, new_value, old_value)
        self.actions_holder.append(action)

        self.box_model.action_param_semaphore = True
        self.box_model.set_parameter(self.parameter, new_value)
        self.box_model.action_param_semaphore = False

    def text_line_text_edited (self) -> None :
        """Est la méthode appelée lorsque la valeur de l'éditeur de texte est modifiée."""

        if self._semaphore_values :
            return

        print('text box value changed')

        new_value = self._widget_value_editor.text()
        old_value = self.box_model.get_parameter(self.parameter)['value']
        param_name = 'params.' + self.parameter

        self.actions_generator(old_value, new_value, param_name)
        
        self.box_model.action_param_semaphore = True
        self.box_model.set_parameter(self.parameter, new_value)
        self.box_model.action_param_semaphore = False