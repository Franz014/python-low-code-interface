#-----------------------------------
# Imports
#-----------------------------------

from typing import List, Any

from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QListWidgetItem

from packages.core.plci_core_data_type import DataType
from packages.interface.graphical_actions.diagram_remove_outputs_from_box_action import DiagramRemoveOutputsFromBoxAction
from packages.interface.models.signals_model.output_connection_model import OutputConnectionModel
from packages.interface.properties_pages.abstract_infinite_io_properties_editor import AbstractInfiniteIOPropertiesEditor
from packages.interface.models.graphical_models.box_model import BoxModel
from packages.interface.properties_pages.infinite_io_properties_item import InifiniteIOPropertiesItem
from packages.interface.graphical_actions.diagram_add_output_to_box_action import DiagramAddOutputToBoxAction

import packages.interface.models.color as c

#-----------------------------------
# Class
#-----------------------------------

class InfiniteOutputPropertiesEditor (AbstractInfiniteIOPropertiesEditor) :
    """Est la classe qui décrit le fonctionnement du panneau pour éditer les propriétés des sorties infinies."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self, box_model : BoxModel, output_name : str, data_type : DataType) :

        AbstractInfiniteIOPropertiesEditor.__init__(self, box_model, output_name, data_type)

        self._output_name = output_name

        count : int = 0
        for output in box_model.outputs :
            if output.is_infinite : 
                if output.name :
                    item = QListWidgetItem(self._tbl_connection)
                    self._tbl_connection.addItem(item)

                    count += 1
                    item_widget = InifiniteIOPropertiesItem(count, output)
                    item.setSizeHint(item_widget.minimumSizeHint())
                    self._tbl_connection.setItemWidget(item, item_widget)

        self._btn_remove.setEnabled(self._tbl_connection.count() > 1)
        self._tbl_connection.setMinimumHeight(self._tbl_connection.itemWidget(self._tbl_connection.item(0)).height() * 3)
        self._box_model.output_count_changed.connect(self.box_model_output_count_changed)

        
    # -------------
    # Methods
    # -------------
    
    def contains_output (self, output : Any) -> int : 
        """Permet de savoir si la sortie est contenue dans la liste."""

        for element_index in range(self._tbl_connection.count()) :
            item = self._tbl_connection.item(element_index)
            item_widget = self._tbl_connection.itemWidget(item)
            item_input = item_widget._connection_model

            if item_input.graphical_index == output.graphical_index :
                return element_index

        return -1

    def box_model_output_count_changed (self) -> None :
        """Est la méthode appelée losque le nombre de sortie le box est modifié."""

        for element_index in range(self._tbl_connection.count() - 1, -1, -1) :
            item = self._tbl_connection.item(element_index)
            item_widget = self._tbl_connection.itemWidget(item)
            output = item_widget._connection_model

            if not(output in self.box_model._outputs) :
                item_widget : InifiniteIOPropertiesItem = self._tbl_connection.itemWidget(item)
                self._tbl_connection.takeItem(self._tbl_connection.indexFromItem(item).row())
                item_widget.deleteLater()

        for output_index in range(self.box_model.output_len - 1, -1, -1) :
            output = self._box_model.get_output(output_index) 
            element_index = self.contains_output(output)

            if element_index == -1 : 
                item = QListWidgetItem(self._tbl_connection)
                self._tbl_connection.addItem(item)
                item_widget = InifiniteIOPropertiesItem(output_index + 1, output)
                self._tbl_connection.setItemWidget(item, item_widget)

        if self._tbl_connection.count() > 0 :
            item.setSizeHint(self._tbl_connection.itemWidget(self._tbl_connection.item(0)).minimumSizeHint())

        self.recompute_connection_numbers()
        self._btn_remove.setEnabled(self._tbl_connection.count() > 1)

    def init_ui (self) -> None :
        super().init_ui()

    def translate_ui (self) -> None :
        super().translate_ui()
        
        self._lbl_connection_mode_value.setText(QCoreApplication.translate(self.objectName(), u"Entrée", None))

    def button_add_click (self) -> None :
        
        action = DiagramAddOutputToBoxAction(
            self._box_model, 
            self._box_model.output_len,
            self._output_name,
            self._data_type,
            '',
            True,
            c.black
        )
        action.do()
        self.actions_holder.append(action)

    def button_remove_click (self) -> None :

        outputs_to_remove : List[OutputConnectionModel] = []
        
        if len(self._tbl_connection.selectedItems()) == 0 : 
            item = self._tbl_connection.item(self._tbl_connection.count() - 1)
            widget = self._tbl_connection.itemWidget(item)
            output = widget.connection_model
            outputs_to_remove.append(output)

        else :
            
            for index in range(len(self._tbl_connection.selectedIndexes())) :
                item = self._tbl_connection.item(index)
                widget = self._tbl_connection.itemWidget(item)
                output = widget.connection_model
                outputs_to_remove.append(output)
        
        action = DiagramRemoveOutputsFromBoxAction(
            self._box_model,
            outputs_to_remove
        )
        action.do()
        self.actions_holder.append(action)

    def unload (self) -> None :
        self._box_model.output_count_changed.disconnect(self.box_model_output_count_changed)