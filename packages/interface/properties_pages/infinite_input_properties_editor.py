#-----------------------------------
# Imports
#-----------------------------------

from typing import List, Any

from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QListWidgetItem

from packages.core.plci_core_data_type import DataType
from packages.interface.graphical_actions.diagram_add_input_to_box_action import DiagramAddInputToBoxAction
from packages.interface.graphical_actions.diagram_remove_inputs_from_box_action import DiagramRemoveInputsFromBoxAction
from packages.interface.models.signals_model.input_connection_model import InputConnectionModel
from packages.interface.properties_pages.abstract_infinite_io_properties_editor import AbstractInfiniteIOPropertiesEditor
from packages.interface.models.graphical_models.box_model import BoxModel
from packages.interface.properties_pages.infinite_io_properties_item import InifiniteIOPropertiesItem

import packages.interface.models.color as c

#-----------------------------------
# Class
#-----------------------------------

class InfiniteInputPropertiesEditor (AbstractInfiniteIOPropertiesEditor) :
    """Est la classe qui décrit le fonctionnement du panneau pour éditer les propriétés des entrées infinies."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self, box_model : BoxModel, input_name : str, data_type : DataType) :

        AbstractInfiniteIOPropertiesEditor.__init__(self, box_model, input_name, data_type)

        self._input_name = input_name

        count : int = 0
        for input in box_model.inputs :
            if input.is_infinite : 
                if input.name :
                    item = QListWidgetItem(self._tbl_connection)
                    self._tbl_connection.addItem(item)

                    count += 1
                    item_widget = InifiniteIOPropertiesItem(count, input)
                    item.setSizeHint(item_widget.minimumSizeHint())
                    self._tbl_connection.setItemWidget(item, item_widget)

        self._btn_remove.setEnabled(self._tbl_connection.count() > 1)
        self._tbl_connection.setMinimumHeight(self._tbl_connection.itemWidget(self._tbl_connection.item(0)).height() * 3)
        self._input_changed_semaphore = False
        self.box_model.input_count_changed.connect(self.box_mode_input_count_changed)

        
    # -------------
    # Methods
    # -------------
    
    def contains_input (self, input : Any) -> int : 
        """Permet de savoir si l'entrée est contenue dans la liste."""

        for element_index in range(self._tbl_connection.count()) :
            item = self._tbl_connection.item(element_index)
            item_widget = self._tbl_connection.itemWidget(item)
            item_input = item_widget._connection_model

            if item_input.graphical_index == input.graphical_index :
                return element_index

        return -1


    def box_mode_input_count_changed (self) -> None :
        """Est la méthode appelée losque le nombre d'entrée le box est modifié."""

        if self._input_changed_semaphore : 
            return

        for element_index in range(self._tbl_connection.count() - 1, -1, -1) :
            item = self._tbl_connection.item(element_index)
            item_widget = self._tbl_connection.itemWidget(item)
            input = item_widget._connection_model

            if not(input in self.box_model._inputs) :
                item_widget : InifiniteIOPropertiesItem = self._tbl_connection.itemWidget(item)
                self._tbl_connection.takeItem(self._tbl_connection.indexFromItem(item).row())
                item_widget.deleteLater()

        for input_index in range(self.box_model.input_len - 1, -1, -1) :
            input = self._box_model.get_input(input_index) 
            element_index = self.contains_input(input)

            if element_index == -1 : 
                item = QListWidgetItem(self._tbl_connection)
                self._tbl_connection.addItem(item)
                item_widget = InifiniteIOPropertiesItem(input_index + 1, input)
                self._tbl_connection.setItemWidget(item, item_widget)

        if self._tbl_connection.count() > 0 :
            item.setSizeHint(self._tbl_connection.itemWidget(self._tbl_connection.item(0)).minimumSizeHint())

        self.recompute_connection_numbers()
        self._btn_remove.setEnabled(self._tbl_connection.count() > 1)

    def translate_ui (self) -> None :
        super().translate_ui()
        
        self._lbl_connection_mode_value.setText(QCoreApplication.translate(self.objectName(), u"Entrée", None))

    def button_add_click (self) -> None :
        
        action = DiagramAddInputToBoxAction(
            self.box_model, 
            self.box_model.input_len, 
            self._input_name, 
            self._data_type, 
            '', 
            True, 
            c.black)
        action.do()
        
        self.actions_holder.append(action)

    def button_remove_click (self) -> None :

        inputs_to_remove : List[InputConnectionModel] = []
        
        if len(self._tbl_connection.selectedItems()) == 0 : 
            item = self._tbl_connection.item(self._tbl_connection.count() - 1)
            widget = self._tbl_connection.itemWidget(item)
            input = widget.connection_model
            inputs_to_remove.append(input)

        else :
            
            for index in range(len(self._tbl_connection.selectedIndexes())) :
                item = self._tbl_connection.item(index)
                widget = self._tbl_connection.itemWidget(item)
                input = widget.connection_model
                inputs_to_remove.append(input)
        
        action = DiagramRemoveInputsFromBoxAction(
            self._box_model,
            inputs_to_remove
        )
        action.do()
        self.actions_holder.append(action)

    def unload (self) -> None :
         self.box_model.input_count_changed.disconnect(self.box_mode_input_count_changed)