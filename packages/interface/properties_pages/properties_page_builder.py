#-----------------------------------
# Imports
#-----------------------------------

from typing import Iterable, Any

from PyQt5.QtCore import QCoreApplication

from packages.interface.properties_pages.abstract_properties_editor import AbstractPropertiesEditor
from packages.interface.properties_pages.box_properties import BoxProperties
from packages.interface.properties_pages.infinite_output_properties_editor import InfiniteOutputPropertiesEditor
from packages.interface.properties_pages.moveable_graphical_element_properties_editor import MoveableGraphicalElementPropertiesEditor
from packages.interface.properties_pages.parameter_properties_editor import ParameterPropertiesEditor
from packages.interface.properties_pages.signal_properties_editor import SignalPropertiesEditor
from packages.interface.properties_pages.title_properties_element import TitlePropertiesElement
from packages.interface.properties_pages.unknown_properties_editor import UnknownPropertiesEditor
from packages.interface.models.graphical_models.abstract_moveable_graphical_element import AbstractMoveableGraphicalElement
from packages.interface.models.graphical_models.box_model import BoxModel
from packages.interface.models.graphical_models.box_input_model import BoxInputModel
from packages.interface.models.graphical_models.box_output_model import BoxOutputModel
from packages.interface.models.signals_model.signal_link_model import SignalLinkModel
from packages.interface.properties_pages.infinite_input_properties_editor import InfiniteInputPropertiesEditor

#-----------------------------------
# Function
#-----------------------------------

def get_properties_page (element : Any) -> Iterable[AbstractPropertiesEditor] :
    
    based_editor_loaded = False

    if isinstance(element, SignalLinkModel) : 
        yield SignalPropertiesEditor(element)
        return
    
    if isinstance(element, BoxOutputModel) :
        properties_page = None

    elif isinstance(element, BoxInputModel) : 
        properties_page = None

    elif isinstance(element, BoxModel) :
        box_model : BoxModel = element
        yield BoxProperties(box_model)

        already_added_name_input = []

        for input in box_model.inputs : 
            if input.is_infinite : 
                if not(input.name in already_added_name_input) :
                    already_added_name_input.append(input.name)
                    yield InfiniteInputPropertiesEditor(box_model, input.name, input.data_type)

        already_added_name_output = []

        for output in box_model.outputs : 
            if output.is_infinite : 
                if not(output.name in already_added_name_output) :
                    already_added_name_output.append(output.name)
                    yield InfiniteOutputPropertiesEditor(box_model, output.name, output.data_type)

        if box_model.get_parameters_len() > 0 :
            yield TitlePropertiesElement(QCoreApplication.translate('properties_page_builder', u"Paramètres : ", None))
            for parameter in box_model.get_parameters() :
                yield ParameterPropertiesEditor(box_model, parameter)

        based_editor_loaded = True


    if based_editor_loaded == False :
        yield UnknownPropertiesEditor()
        return

    else :

        if isinstance(element, AbstractMoveableGraphicalElement) :
            yield MoveableGraphicalElementPropertiesEditor(element)
            