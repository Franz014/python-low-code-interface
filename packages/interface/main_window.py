#-----------------------------------
# Imports
#-----------------------------------

import os
import time
from typing import List, Any, Dict

import matplotlib.pylab as plt

from PyQt5 import QtGui
from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtWidgets import QAction, QLabel, QMainWindow, QMenuBar, QSplitter, QToolBox, QWidget, QFileDialog
from PyQt5.QtGui import QKeySequence

from packages.core.plci_core_scheduler import Scheduler
from packages.core.plci_core_scheduler_exception import SchedulerException
from packages.interface.box_library.box_libraries_list import BoxLibrariesList
from packages.interface.models.editable_models.scheduler_model import SchedulerModel
from packages.interface.properties_pages.properties_widget import PropertiesWidget
from packages.interface.dialogs.abstract_dialog import AbstractDialog
from packages.interface.dialogs.dialogs_holder import DialogsHolder
from packages.interface.editors.factory_editors import factory_editors
from packages.interface.models.editable_models.abstract_editable_model import AbstractEditableModel
from packages.interface.models.editable_models.simulation_model import SimulationModel
from packages.interface.project_tabs_holder import ProjectTabsHolder
from packages.interface.recent_file_menu import RecentFileMenu
from packages.interface.user_data.user_data import UserData
from packages.interface.new_project_at_startup import new_project_on_startup
from packages.core.box_library.plci_core_boxes_libraries import BoxesLibraries
from packages.core.scheduler_library.plci_core_schedulers_libraries import SchedulersLibraries
from packages.parser.parser import Parser

import packages.interface.files.files as files


#-----------------------------------
# Class
#-----------------------------------

class MainWindow (QMainWindow) :
    """Est la fenêtre principale."""
    
    # -------------
    # Constructors
    # -------------

    def __init__ (self) :
        super(MainWindow, self).__init__()
        
        self.setGeometry(100, 100, 1024, 768)
        self.setWindowTitle("MuPhyN")

        self._boxes_libraries : BoxesLibraries = BoxesLibraries()
        self._schedulers_libraries : SchedulersLibraries = SchedulersLibraries()
        
        self._user_data : UserData = UserData("./user_data.yaml")
        self._user_data.load()

        for box_library in self._user_data.boxes_libraries :
            self._boxes_libraries.add_library(box_library)

        for scheduler_library in self._user_data.schedulers_libraries :
            self._schedulers_libraries.add_library(scheduler_library)

        self._boxes_libraries.load_libraries()
        self._schedulers_libraries.load_libraries()

        self._dialogs_holder : DialogsHolder = DialogsHolder(self)
        self._dialogs_holder.dialog_opened_listener.append(self._dialog_opened_event)
        self._dialogs_holder.dialog_closed_listener.append(self._dialog_closed_event)
        self._dialogs_holder.dialog_closed_all_listener.append(self._dialogs_closed_all_event)

        self._opened_dialogs_actions : Dict[AbstractDialog, QAction] = {}

        self._init_actions()
        self._init_menu()
        self._init_ui()

        self.setAcceptDrops(True)

        self.regenerate_recent_files()
        self.open(new_project_on_startup(self._user_data, self._schedulers_libraries))

    # -------------
    # Properties
    # -------------

    @property
    def dialogs_holder (self) -> DialogsHolder :
        """Permet de récuperer l'objet contenant les dialogues."""
        return self._dialogs_holder

    @property
    def boxes_libraries (self) -> BoxesLibraries :
        """Permet de récuperer l'objet contenant les bibliothèques de boxes."""
        return self._boxes_libraries

    @property
    def schedulers_libraries (self) -> SchedulersLibraries :
        """Permet de récuperer l'objet contenant les bibliothèques de planificateurs."""
        return self._schedulers_libraries


    # -------------
    # Methods
    # -------------

    def dropEvent (self, drop_event : QtGui.QDropEvent) -> None :

        if drop_event.mimeData().hasUrls() :

            for url in drop_event.mimeData().urls() :
                self.open_file(url.toLocalFile())

        super().dropEvent(drop_event)

    def dragEnterEvent (self, drag_enter_event : QtGui.QDragEnterEvent) -> None :

        if drag_enter_event.mimeData().hasUrls() :
            drag_enter_event.accept()

        super().dragEnterEvent(drag_enter_event)

    def dragMoveEvent (self, drag_move_event : QtGui.QDragMoveEvent) -> None :
        return super().dragMoveEvent(drag_move_event)
    
    def open_file (self, path : str) -> None :
        """Permet d'ouvrir un fichier."""

        if os.path.exists(path) :
            editable_model : AbstractEditableModel = files.load(path)

            if editable_model is None : 
                print('no file to load !!')
                return

            else :
                self.open(editable_model)

    def open (self, adm : AbstractEditableModel) -> None :
        """Permet d'ouvrir un model d'édition."""

        for graphical_editor_index in range(self._tabs.count()) :
            graphical_editor : AbstractEditableModel = self._tabs.widget(graphical_editor_index)

            if graphical_editor.editable_model.name == adm.name and graphical_editor.editable_model.path == adm.path :
                return
        
        tab : QWidget = factory_editors(self._tabs, adm)
        
        if isinstance(tab, QLabel) :
            self._tabs.addTab(tab, adm.name)

        else :
            self._tabs.addEditor(tab)

        if self._tabs.count() > 1 :
            self._tabs.setCurrentIndex(self._tabs.count() - 1)

        self.add_recent_file(adm.path + adm.name + '.yaml')

    def add_recent_file (self, path : str) -> None :
        """Permet d'essayer d'ajouter le fichier dans les fichiers récents."""

        if path in self._user_data.recent_files :
            self._user_data.recent_files.remove(path)
        
        self._user_data.recent_files.insert(0, path)
        self.regenerate_recent_files()

    def regenerate_recent_files (self) -> None : 
        """Permet de regénérer le menu des fichiers récents."""
        max_files = 10
        current_file_number = 0
        for action_index in range(len(self._mn_recent_files.actions())).__reversed__() :
            self._mn_recent_files.removeAction(self._mn_recent_files.actions()[action_index])

        for path in self._user_data.recent_files :

            if current_file_number >= max_files : 
                return

            if os.path.exists(path) :
                mn_recent_file_action = RecentFileMenu(self, path, self.open_file)
                self._mn_recent_files.addAction(mn_recent_file_action)

                current_file_number += 1

    @pyqtSlot()
    def _file_open_method (self) -> None :
        """Est la méthode appelée lorsque l'utilisateur veut ouvrir un fichier."""

        path = QFileDialog.getOpenFileName(self, 'Ouvrir', os.getcwd(), 'YAML (*.yaml)')[0]
        if path is None or len(path) == 0 :
            return

        self.open_file(path)

    @pyqtSlot()
    def _file_save_method (self) -> None :
        """Est la méthode appelée lorsque l'utilisateur veut sauvegarder le fichier en cours."""

        if self._tabs.current_editor is None : 
            return

        if self._tabs.current_editor.editable_model is None : 
            return
        
        file_name = self._tabs.current_editor.editable_model.path.__str__()
        if not(file_name.endswith(os.sep)) :
            file_name = file_name + os.sep

        file_name = file_name + self._tabs.current_editor.editable_model.name

        if not(file_name.endswith('.yaml')):
            file_name = file_name + '.yaml'

        files.save(self._tabs.current_editor.editable_model, file_name)

    @pyqtSlot()
    def _file_save_as_method (self) -> None :
        """Est la méthode appelée lorsque l'utilisateur veut sauvegarder le fichier en cours sous un nouveau nom."""

        if self._tabs.current_editor is None : 
            return

        if self._tabs.current_editor.editable_model is None : 
            return

        path = QFileDialog.getSaveFileName(self, 'Sauvegarder', os.getcwd(), 'YAML (*.yaml)')[0]

        if path is None or len(path) == 0 :
            return

        if files.save(self._tabs.current_editor.editable_model, path) : 

            file_name = os.path.basename(path)
            abs_path = path[:-file_name.__len__()]
            file_name = file_name[:-('.yaml').__len__()]

            self._tabs.current_editor.editable_model.set_path_name(abs_path, file_name)

    @pyqtSlot()
    def _file_close_method (self) -> None : 
        """Est la méthode appelée pour fermer le fichier actuel."""
        self.tab_close_request(self._tabs.currentIndex())

    @pyqtSlot()
    def _undo_method (self) -> None :
        """Est la méthode appelée pour défaire la dernière action."""

        if self._tabs.current_editor is None : 
            return

        self._tabs.current_editor.actions_holder.undo()

    @pyqtSlot()
    def _redo_method (self) -> None : 
        """Est la méthode appelée pour refaire la dernière action défaite."""

        if self._tabs.current_editor is None : 
            return

        self._tabs.current_editor.actions_holder.redo()

    @pyqtSlot()
    def _window_libraries_method (self) -> None :
        """Est la méthode appelée pour afficher un dialogue permettant à l'utilisateur de modifier les bibliothèques."""
        self.dialogs_holder.show_dialog('library', True, boxes_libraries = self._boxes_libraries, schedulers_libraries = self._schedulers_libraries)

    @pyqtSlot()
    def _help_documentation_method (self) -> None :
        """Est la méthode appelée pour afficher un dialogue permettant d'aider l'utilisateur."""
        self.dialogs_holder.show_dialog('documentation', False)

    @pyqtSlot()
    def _help_about_method (self) -> None :
        """Est la méthode appelée pour afficher un dialogue affichant les détails des licences et des personnes ayant travaillés sur le programme."""
        self.dialogs_holder.show_dialog('about', False)

    @pyqtSlot()
    def _file_new_simulation_method (self) -> None :
        """Est la méthode appellée lorsque l'utilisateur veut créer une nouvelle simulation."""
        self.dialogs_holder.show_dialog('new_simulation', True, schedulers_libraries = self._schedulers_libraries)

    @pyqtSlot()
    def _simulation_parameters_method (self) -> None :
        """Est la méthode appelée pour afficher un digalogue permettant de modifier les paramètres de la simulation."""
        if isinstance(self._tabs.current_editor.editable_model, SimulationModel) : 
            self.dialogs_holder.show_dialog('simulation_parameters_dialog', True, current_simulation = self._tabs.current_editor.editable_model, schedulers_libraries = self._schedulers_libraries)

    @pyqtSlot()
    def _simulation_launch_method (self) -> None :
        """Est la méthode appelée pour lancer la simulation en cours d'édition."""
        
        if self._tabs.current_editor is None : 
            return

        if self._tabs.current_editor.editable_model is None : 
            return

        parser = Parser()
        scheduler : Scheduler = parser.parse(self._tabs.current_editor.editable_model, self.boxes_libraries, self.schedulers_libraries)
        
        if scheduler is None : 
            print('No scheduler returned for the simulation !!')

        else :
            start_time = time.time()
            schedulerException : SchedulerException = scheduler.schedule()
            stop_time = time.time()

            if schedulerException is None :
                print("The simulation is correctly finished and it took : ", stop_time - start_time, " s.")

                plt.show()
                if len(plt.get_fignums()) > 0 :
                    print('figures shown : ')
                    for i in plt.get_fignums() :
                        print(' - ', i)

            else :
                print("The simulation encoutered an error after ", stop_time - start_time, " s of simulation.")
                schedulerException.print()

    @pyqtSlot()
    def _file_new_box_method (self) -> None :
        """Est la méthode appellée lorsque l'utilisateur veut créer une nouvelle box."""
        self.dialogs_holder.show_dialog('new_box', True, user_data = self._user_data)

    @pyqtSlot()
    def _file_new_solver_method (self) -> None :
        """Est la méthode appellée lorsque l'utilisateur veut créer un nouveau solveur."""
        self.dialogs_holder.show_dialog('new_scheduler', True, user_data = self._user_data)

    @pyqtSlot()
    def _edit_copy_method (self) -> None :
        """Est la méthode appelée pour copier la sélection actuelle."""
        if self._tabs.current_editor is None : 
            return

        self._tabs.current_editor.copy()

    @pyqtSlot()
    def _edit_cut_method (self) -> None :
        """Est la méthode appelée pour couper la sélection actuelle."""
        if self._tabs.current_editor is None : 
            return

        self._tabs.current_editor.cut()

    @pyqtSlot()
    def _edit_paste_method (self) -> None :
        """Est la méthode appelée pour coller le contenu du clipboard."""
        if self._tabs.current_editor is None : 
            return

        self._tabs.current_editor.paste()

    @pyqtSlot()
    def _edit_properties_method (self) -> None :
        """Est la méthode appelée pour modifier les propriétés de la box sélectionnée."""
        self._tool_box.setCurrentWidget(self._tool_box_page_properties)

    @pyqtSlot()    
    def _edit_rot_clock_90_method (self) -> None :
        """Est la méthode appelée pour faire tourner la box sélectionné de 90° dans le sens des aiguilles d'une montre."""
        
        current_editor = self._tabs.current_editor

        if current_editor is None :
            return

        for element in current_editor.selected_elements() :
            if hasattr(element, 'setRotation') :
                element.setRotation(element.rotation() + 90)

        
    @pyqtSlot()
    def _edit_rot_anti_clock_90_method (self) -> None :
        """Est la méthode appelée pour faire tourner la box sélectionné de 90° dans le sens des aiguilles d'une montre."""
        
        current_editor = self._tabs.current_editor

        if current_editor is None :
            return

        for element in current_editor.selected_elements() :
            if hasattr(element, 'setRotation') :
                element.setRotation(element.rotation() - 90)

    @pyqtSlot()
    def _edit_rot_reverse_method (self) -> None :
        """Est la méthode appelée pour retourner la box sélectionné."""
        
        current_editor = self._tabs.current_editor

        if current_editor is None :
            return

        for element in current_editor.selected_elements() :
            if hasattr(element, 'setRotation') :
                element.setRotation(element.rotation() + 180)

    @pyqtSlot()
    def _edit_select_all_method (self) -> None :
        """Est la méthode appelée pour sélectionner tous les éléments."""
        
        current_editor = self._tabs.current_editor

        if current_editor is None :
            return

        for element in current_editor.elements() :
            element.setSelected(True)

    @pyqtSlot()
    def _edit_reverse_selection_method (self) -> None :
        """Est la méthode appelée pour inverser la sélection des éléments."""
        current_editor = self._tabs.current_editor

        if current_editor is None :
            return

        for element in current_editor.elements() :
            element.setSelected(not(element.isSelected()))
    
    @pyqtSlot()
    def _edit_erase_selection_method (self) -> None :
        """Est la méthode appelée pour supprimer la sélection."""
        current_editor = self._tabs.current_editor

        if current_editor is None :
            return
        
        current_editor.delete_selection()
    
    @pyqtSlot()
    def _window_zoom_method (self) -> None : 
        """Est la méthode appelée pour zoomer dans l'interface d'édition."""
        
        current_editor = self._tabs.current_editor

        if current_editor is None :
            return

        current_editor.zoom(1)
    
    @pyqtSlot()
    def _window_dezoom_method (self) -> None :
        """Est la méthode appelée pour dézoomer dans l'interface d'édition."""
        
        current_editor = self._tabs.current_editor

        if current_editor is None :
            return

        current_editor.zoom(-1)

    def _init_actions (self) -> None :
        """Permet de créer les actions que la fenêtres peut réaliser."""

        self._file_new_simulation_action = QAction('&Simulation', self)
        self._file_new_simulation_action.triggered.connect(self._file_new_simulation_method)
        self._file_new_simulation_action.setShortcut(QKeySequence('Ctrl+N'))

        self._file_new_box_action = QAction('&Box', self)
        self._file_new_box_action.triggered.connect(self._file_new_box_method)
        self._file_new_box_action.setShortcut(QKeySequence('Ctrl+Shift+N'))

        self._file_new_solver_action = QAction('S&olveur', self)
        self._file_new_solver_action.triggered.connect(self._file_new_solver_method)

        self._file_open_action = QAction('&Ouvrir', self)
        self._file_open_action.triggered.connect(self._file_open_method)
        self._file_open_action.setShortcut(QKeySequence('Ctrl+O'))
        
        self._file_save_action = QAction('&Sauvegarder', self)
        self._file_save_action.triggered.connect(self._file_save_method)
        self._file_save_action.setShortcut(QKeySequence('Ctrl+S'))

        self._file_save_as_action = QAction('S&auvegarder sous', self)
        self._file_save_as_action.triggered.connect(self._file_save_as_method)
        self._file_save_as_action.setShortcut(QKeySequence('Ctrl+Shift+S'))

        self._file_export_action = QAction('&Exporter', self)
        self._file_export_action.triggered.connect(self._file_export_method)
        self._file_export_action.setShortcut(QKeySequence('Ctrl+E'))

        self._file_close_action = QAction('&Fermer', self)
        self._file_close_action.triggered.connect(self._file_close_method)
        self._file_close_action.setShortcut(QKeySequence('Ctrl+W'))

        self._file_exit_action = QAction('&Quitter', self)
        self._file_exit_action.triggered.connect(self.close)
        self._file_exit_action.setShortcut(QKeySequence('Alt+F4'))

        self._edit_undo_action = QAction('&Undo', self)
        self._edit_undo_action.triggered.connect(self._undo_method)
        self._edit_undo_action.setShortcut(QKeySequence('Ctrl+Z'))

        self._edit_redo_action = QAction('&Redo', self)
        self._edit_redo_action.triggered.connect(self._redo_method)
        self._edit_redo_action.setShortcut(QKeySequence('Ctrl+Y'))

        self._edit_cut_action = QAction('C&ouper', self)
        self._edit_cut_action.triggered.connect(self._edit_cut_method)
        self._edit_cut_action.setShortcut(QKeySequence('Ctrl+X'))

        self._edit_copy_action = QAction('&Copier', self)
        self._edit_copy_action.triggered.connect(self._edit_copy_method)
        self._edit_copy_action.setShortcut(QKeySequence('Ctrl+C'))

        self._edit_paste_action = QAction('Co&ller', self)
        self._edit_paste_action.triggered.connect(self._edit_paste_method)
        self._edit_paste_action.setShortcut(QKeySequence('Ctrl+V'))

        self._edit_properties_action = QAction('Propriétés', self)
        self._edit_properties_action.triggered.connect(self._edit_properties_method)

        self._rot_clock_90_action = QAction('90° horloger', self)
        self._rot_clock_90_action.triggered.connect(self._edit_rot_clock_90_method)

        self._rot_anti_clock_90_action = QAction('90° Anti horloger', self)
        self._rot_anti_clock_90_action.triggered.connect(self._edit_rot_anti_clock_90_method)

        self._rot_reverse_action = QAction('Retourner', self)
        self._rot_reverse_action.triggered.connect(self._edit_rot_reverse_method)

        self._select_all_action = QAction('Sélectionner tout', self)
        self._select_all_action.triggered.connect(self._edit_select_all_method)
        self._select_all_action.setShortcut(QKeySequence('Ctrl+A'))

        self._reverse_selection_action = QAction('Invérser la sélection', self)
        self._reverse_selection_action.triggered.connect(self._edit_reverse_selection_method)
        self._reverse_selection_action.setShortcut(QKeySequence('Ctrl+Shift+A'))

        self._edit_new_simulation_from_selection_action = QAction('Créer une simulation depuis la sélection', self)
        self._edit_new_simulation_from_selection_action.triggered.connect(self._edit_new_simulation_from_selection_method)

        self._edit_new_composite_box_from_selection_action = QAction('Créer une box composite depuis la sélection', self)
        self._edit_new_composite_box_from_selection_action.triggered.connect(self._edit_new_composite_box_from_selection_method)

        self._edit_erase_selection_action = QAction('Supprimer', self)
        self._edit_erase_selection_action.triggered.connect(self._edit_erase_selection_method)
        self._edit_erase_selection_action.setShortcut(QKeySequence('Delete'))

        self._window_zoom_action = QAction('Zoom +', self)
        self._window_zoom_action.triggered.connect(self._window_zoom_method)
        self._window_zoom_action.setShortcut(QKeySequence('Ctrl++'))

        self._window_dezoom_action = QAction('Zoom -', self)
        self._window_dezoom_action.triggered.connect(self._window_dezoom_method)
        self._window_dezoom_action.setShortcut(QKeySequence('Ctrl+-'))

        self._window_close_all_action = QAction('Fermer les dialogues', self)
        self._window_close_all_action.triggered.connect(self._dialogs_holder.close_all)
        self._window_close_all_action.setEnabled(self._dialogs_holder.len > 0)

        self._window_libraries_action = QAction('Bibliothèques', self)
        self._window_libraries_action.triggered.connect(self._window_libraries_method)
        
        self._window_options_action = QAction('Options', self)
        self._window_options_action.triggered.connect(self._window_options_method)

        self._simulation_parameters_action = QAction('Paramètres', self)
        self._simulation_parameters_action.triggered.connect(self._simulation_parameters_method)

        self._simulation_launch_action = QAction('Lancer', self)
        self._simulation_launch_action.triggered.connect(self._simulation_launch_method)
        self._simulation_launch_action.setShortcut(QKeySequence('F5'))

        self._simulation_stop_action = QAction('Stopper', self)
        self._simulation_stop_action.triggered.connect(self._simulation_stop_method)
        self._simulation_stop_action.setShortcut(QKeySequence('Shift+F5'))

        self._help_documentation_action = QAction('&Documentation', self)
        self._help_documentation_action.triggered.connect(self._help_documentation_method)
        self._help_documentation_action.setShortcut(QKeySequence('F1'))
        
        self._help_about_action = QAction('&À propos', self)
        self._help_about_action.triggered.connect(self._help_about_method)

    def _init_menu (self) -> None : 
        """Permet de créer le menu afficher dans la fenêtre."""

        self._menu_bar = QMenuBar()
        self.setMenuBar(self._menu_bar)

        self._mn_file = self._menu_bar.addMenu('&Fichiers')
        self._mn_file_new = self._mn_file.addMenu('&Nouveau')
        self._mn_file_new.addAction(self._file_new_simulation_action)
        self._mn_file_new.addAction(self._file_new_box_action)
        self._mn_file_new.addAction(self._file_new_solver_action)
        self._mn_file.addAction(self._file_open_action)
        self._mn_file.addAction(self._file_save_action)
        self._mn_file.addAction(self._file_save_as_action)
        self._mn_recent_files = self._mn_file.addMenu('Fichiers récents')
        self._mn_file.addSeparator()
        self._mn_file.addAction(self._file_export_action)
        self._mn_file.addSeparator()
        self._mn_file.addAction(self._file_close_action)
        self._mn_file.addSeparator()
        self._mn_file.addAction(self._file_exit_action)

        self._mn_edit = self._menu_bar.addMenu('&Édition')
        self._mn_edit.addAction(self._edit_undo_action)
        self._mn_edit.addAction(self._edit_redo_action)
        self._mn_edit.addSeparator()
        self._mn_edit.addAction(self._edit_cut_action)
        self._mn_edit.addAction(self._edit_copy_action)
        self._mn_edit.addAction(self._edit_paste_action)
        self._mn_edit.addSeparator()
        self._mn_edit.addAction(self._edit_properties_action)
        self._mn_edit_rotation = self._mn_edit.addMenu('Rotation')
        self._mn_edit_rotation.addAction(self._rot_clock_90_action)
        self._mn_edit_rotation.addAction(self._rot_anti_clock_90_action)
        self._mn_edit_rotation.addAction(self._rot_reverse_action)
        self._mn_edit.addSeparator()
        self._mn_edit.addAction(self._select_all_action)
        self._mn_edit.addAction(self._reverse_selection_action)
        self._mn_edit.addSeparator()
        self._mn_edit.addAction(self._edit_new_simulation_from_selection_action)
        self._mn_edit.addAction(self._edit_new_composite_box_from_selection_action)
        self._mn_edit.addSeparator()
        self._mn_edit.addAction(self._edit_erase_selection_action)

        self._mn_window = self._menu_bar.addMenu('&Affichage')
        self._mn_window.addAction(self._window_zoom_action)
        self._mn_window.addAction(self._window_dezoom_action)
        self._mn_window.addSeparator()
        self._mn_window_dialogues = self._mn_window.addMenu('Dialogue ouvertes')
        self._mn_window_dialogues.setEnabled(self._dialogs_holder.len > 0)
        self._mn_window.addAction(self._window_close_all_action)
        self._mn_window.addSeparator()
        self._mn_window.addAction(self._window_libraries_action)
        self._mn_window.addAction(self._window_options_action)
        
        self._mn_simulation = self._menu_bar.addMenu('&Simulation')
        self._mn_simulation.addAction(self._simulation_parameters_action)
        self._mn_simulation.addSeparator()
        self._mn_simulation.addAction(self._simulation_launch_action)
        self._mn_simulation.addAction(self._simulation_stop_action)

        self._mn_help = self._menu_bar.addMenu('&?')
        self._mn_help.addAction(self._help_documentation_action)
        self._mn_help.addSeparator()
        self._mn_help.addAction(self._help_about_action)

    def tabs_elements_selected_changed (self, elements_i) -> None :
        """Est la méthode appelée lorsque l'utilisateur modifie sa sélection."""

        current_editor = self._tabs.current_editor

        if current_editor is None :
            return
        
        elements_l : List = []
        for element in elements_i() : 
            elements_l.append(element)

        if elements_l.__len__() == 1 :
            self._tool_box_page_properties.actions_holder = current_editor.actions_holder
            self._tool_box_page_properties.current_model = elements_l[0]

        else :
            self._tool_box_page_properties.actions_holder = None
            self._tool_box_page_properties.current_model = None
    
    def _init_ui (self) :
        """Est la méthode appelée pour afficher les éléments visuels."""

        self._splt_main_window = QSplitter(Qt.Horizontal)
        self._tabs = ProjectTabsHolder(self._splt_main_window)
        self._tool_box = QToolBox(self._splt_main_window)
        self._libraries = BoxLibrariesList(self._tool_box, self._boxes_libraries)
        self._tool_box_page_properties = PropertiesWidget(self._tool_box)

        self._tabs.elements_selected_changed.connect(self.tabs_elements_selected_changed)
        self._tabs.tabCloseRequested.connect(self.tab_close_request)

        self._tool_box.addItem(self._libraries, 'Bibliothèques')
        self._tool_box.addItem(self._tool_box_page_properties, 'Propritétés')
        self._tool_box.setContentsMargins(0, 0, 5, 5)

        self._splt_main_window.addWidget(self._tabs)
        self._splt_main_window.addWidget(self._tool_box)
        self._splt_main_window.setSizes([100,50])
        self.setCentralWidget(self._splt_main_window)

    @pyqtSlot(int)
    def tab_close_request (self, index : int = ...) :
        
        if self._tabs.count() == 1 :
            return

        current_widget = self._tabs.widget(index)
        self._tabs.removeTab(index)
        current_widget.deleteLater()


    def closeEvent(self, event: QtGui.QCloseEvent) -> None:
        """Est la méthode appelée à la fermeture de la fenêtre."""
        super().closeEvent(event)

        self._user_data.save()

    def _dialog_opened_event (self, abstract_dialog : AbstractDialog) -> None :
        """Est la méthode appelée lorsqu'un dialogue est ouvert."""

        self._mn_window_dialogues.setEnabled(self._dialogs_holder.len > 0)
        self._window_close_all_action.setEnabled(self._dialogs_holder.len > 0)

        new_dialog_action : QAction = QAction(abstract_dialog.windowTitle(), self)
        new_dialog_action.triggered.connect(lambda : abstract_dialog.close())
        self._opened_dialogs_actions[abstract_dialog] = new_dialog_action
        self._mn_window_dialogues.addAction(new_dialog_action)

    def _dialog_closed_event (self, abstract_dialog : AbstractDialog, arg : Any) -> None :
        """Est la méthode appelée lorsqu'un dialogue est fermé.""" 

        self._mn_window_dialogues.setEnabled(self._dialogs_holder.len > 0)
        self._window_close_all_action.setEnabled(self._dialogs_holder.len > 0)

        self._mn_window_dialogues.removeAction(self._opened_dialogs_actions[abstract_dialog])
        del self._opened_dialogs_actions[abstract_dialog]

        if abstract_dialog.name == 'library':
            self._user_data.schedulers_libraries.clear()
            self._user_data.boxes_libraries.clear()

            for library in self._boxes_libraries._libraries:
                if not library.path in self._user_data.boxes_libraries :
                    self._user_data.boxes_libraries.append(library.path)

            self._libraries.libraries_reloaded()

            for library in self._schedulers_libraries._libraries:
                if not library.path in self._user_data.schedulers_libraries :
                    self._user_data.schedulers_libraries.append(library.path)

        elif abstract_dialog.name == 'new_simulation' :

            if abstract_dialog.value is None :
                return

            self.open(abstract_dialog.value)

        elif abstract_dialog.name == 'new_box' :

            if abstract_dialog.value is None :
                return

            self.open(abstract_dialog.value)

        elif abstract_dialog.name == 'simulation_parameters_dialog' :

            if abstract_dialog.value is None : 
                return

            if self._tabs.current_editor is None : 
                return

            if not(isinstance(self._tabs.current_editor.editable_model, SimulationModel)) :
                return

            if isinstance(abstract_dialog.value, SchedulerModel) :
                self._tabs.current_editor.editable_model.scheduler_model = abstract_dialog.value

        else :
            print('dialog closed not supported : ', abstract_dialog.name)


    def _dialogs_closed_all_event (self, dialogs_holder : DialogsHolder) -> None :
        """Est la méthode appelée lorsque toutes les boite de dialogues ont été fermée."""
        
        self._mn_window_dialogues.setEnabled(self._dialogs_holder.len > 0)
        self._window_close_all_action.setEnabled(self._dialogs_holder.len > 0)

        for dialog in self._opened_dialogs_actions :
            self._mn_window_dialogues.removeAction(self._opened_dialogs_actions[dialog])

        self._opened_dialogs_actions.clear()

    @pyqtSlot()
    def _file_export_method (self) -> None :
        """Est la méthode appelée pour exporter le fichier actuel."""
        print('Not implemented : Export')
    
    @pyqtSlot()
    def _edit_new_simulation_from_selection_method (self) -> None :
        """Est la méthode appelée pour créer une nouvelle simulation depuis la sélection."""
        print('Not implemented : New simulation from selection')

    @pyqtSlot()
    def _edit_new_composite_box_from_selection_method (self) -> None :
        """Est la méthode appelée pour créer une nouvelle box composite depuis la sélection."""
        print('Not implemented : New composite box from selection')
        
    @pyqtSlot()
    def _window_options_method (self) -> None :
        """Est la méthode appelée pour afficher un dialogue permettant à l'utilisateur de gérer les options du programme."""
        print('Not implemented : Show options popup')

    @pyqtSlot()
    def _simulation_stop_method (self) -> None :
        """Est la méthode appelée pour stopper l'éventuel simulation en cours de simulation."""
        print('Not implemented : Stop simulate')