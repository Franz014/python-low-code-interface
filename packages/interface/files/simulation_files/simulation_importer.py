#-----------------------------------
# Imports
#-----------------------------------

from typing import Dict

from packages.core.plci_core_scheduler_params import SchedulerParams
from packages.interface.files.abstract_importer import AbstractImporter
from packages.interface.models.color import Color
from packages.interface.models.editable_models.scheduler_model import SchedulerModel
from packages.interface.models.editable_models.simulation_model import SimulationModel
from packages.interface.models.graphical_models.box_model import BoxModel
from PyQt5.QtCore import QSizeF, QPointF
from packages.core.plci_core_data_type import get_data_type
from packages.interface.models.links_model.link_type import get_link_type
from packages.interface.models.signals_model.input_connection_model import InputConnectionModel
from packages.interface.models.signals_model.output_connection_model import OutputConnectionModel

#-----------------------------------
# Functions
#-----------------------------------

def construct_box (box_dict : Dict) -> BoxModel :
    """Est la méthode appelée pour produire un modèle de box suivant un dictionnaire d'import."""

    library = box_dict['library']
    name = box_dict['name']
    position = QPointF(float(box_dict['geometry']['x']), float(box_dict['geometry']['y']))
    size = QSizeF(float(box_dict['geometry']['width']), float(box_dict['geometry']['height']))
    rotation = float(box_dict['geometry']['rotation'])
    text = box_dict['text']

    box_model = BoxModel(library, name, position, size, rotation, True, text)

    for parameter_data in box_dict['params'] :
        box_model.create_parameter(parameter_data, box_dict['params'][parameter_data]['type'], box_dict['params'][parameter_data]['value'])

    return box_model


def construct_input (input_dict : Dict, current_input_index : int, box_model : BoxModel) -> InputConnectionModel :
    """Est la méthode appelée pour produire un modèle d'entrée d'une box suivant un dictionnaire d'import."""

    input_name = input_dict['name']
    input_type = get_data_type(input_dict['data_type'])
    input_color = Color(float(input_dict['color']['red']), float(input_dict['color']['green']), float(input_dict['color']['blue']))
    input_text = input_dict['text']
    input_is_infinite = input_dict['is_infinite']

    return box_model.insert_input(current_input_index, input_name, input_type, input_text, input_is_infinite, input_color)

def construct_output (output_dict : Dict, current_output_index : int, box_model : BoxModel) -> OutputConnectionModel : 
    """Est la méthode appelée pour produire un modèle de sortie d'une box suivant un dictionnaire d'import."""

    output_name = output_dict['name']
    output_type = get_data_type(output_dict['data_type'])
    output_color = Color(float(output_dict['color']['red']), float(output_dict['color']['green']), float(output_dict['color']['blue']))
    output_text = output_dict['text']
    output_is_infinite = output_dict['is_infinite']

    return box_model.insert_output(current_output_index, output_name, output_type, output_text, output_is_infinite, output_color)

#-----------------------------------
# Class
#-----------------------------------

class SimulationsImporter (AbstractImporter) :
    """Est la classe qui permet d'importer une simulation."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self) :
        
        AbstractImporter.__init__(self)

    # -------------
    # Methods
    # -------------

    def open (self, simulation_data : Dict, path : str, name : str) -> SimulationModel :
    
        if simulation_data['export_version'] == '1.0.0' :
            return self.v_1_0_0(simulation_data, path, name)

        return None

    def v_1_0_0 (self, simulation_data : Dict, path : str, name : str) -> SimulationModel : 
        
        date_creation = simulation_data['date_creation']
        version = float(simulation_data['version'])
        creator = simulation_data['creator']
        stop_time = float(simulation_data['stop_time'])
        step_time = float(simulation_data['step_time'])
        scheduler_library = simulation_data['scheduler']['library']
        scheduler_name = simulation_data['scheduler']['name']

        scheduler_params = SchedulerParams(stop_time, step_time)
        scheduler_model = SchedulerModel(scheduler_library, scheduler_name, scheduler_params)
        simulation_model = SimulationModel(name, path, creator, date_creation, version, scheduler_model)

        boxes = simulation_data['diagram']['boxes']
        signals = simulation_data['diagram']['signals']

        #Ajout de input et ouput au dictionnaire importé dans le but de connecter correctement les noeuds.
        #Plus rapide que de copier l'éxistant et d'ajotuer les "input" et "output". 
        
        for signal in signals : 
            signal['input'] = None
            signal['output'] = None

        for box_data in boxes :

            box_model = construct_box(box_data)
            
            current_input_index = 0
            for input_data in box_data['inputs'] : 

                input = construct_input(input_data, current_input_index, box_model)
                
                input_signal_index = int(input_data['signal_index'])
                if input_signal_index < len(signals) and input_signal_index >= 0 : 

                    signal_data = signals[input_signal_index]
                    if signal_data['input'] is None : 
                        signal_data['input'] = input
                        if not(signal_data['output'] is None) : 
                            simulation_model.link_nodes(input, signal_data['output'], -1, -1,
                                float(signal_data['link_value']), get_link_type(signal_data['link_type']), '')

                current_input_index += 1

            current_output_index = 0
            for output_data in box_data['outputs'] :

                output = construct_output(output_data, current_output_index, box_model)
                
                for output_signal_index in output_data['signal_indices'] : 
                    if output_signal_index < len(signals) and output_signal_index >= 0 : 
                        
                        signal_data = signals[output_signal_index]
                        if signal_data['output'] is None : 
                            signal_data['output'] = output
                            if not(signal_data['input'] is None) : 
                                simulation_model.link_nodes(signal_data['input'], output, -1, -1,
                                    float(signal_data['link_value']), get_link_type(signal_data['link_type']), '')
                
                current_output_index += 1

            simulation_model.add_element(box_model)

        return simulation_model