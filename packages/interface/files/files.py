#-----------------------------------
# Imports
#-----------------------------------

import os
import yaml
from packages.interface.files.abstract_exporter import AbstractExporter
from packages.interface.files.abstract_importer import AbstractImporter
from packages.interface.files.simulation_files.simulation_exporter import SimulationExporter
from packages.interface.files.simulation_files.simulation_importer import SimulationsImporter
from packages.interface.models.editable_models.abstract_editable_model import AbstractEditableModel
from packages.interface.models.editable_models.box_code_model import BoxCodeModel
from packages.interface.models.editable_models.box_composite_model import BoxCompositeModel
from packages.interface.models.editable_models.scheduler_model import SchedulerModel 
from packages.interface.models.editable_models.simulation_model import SimulationModel
from packages.interface.editors.abstract_editor import AbstractEditor

#-----------------------------------
# Functions
#-----------------------------------

def load (path : str) -> AbstractEditableModel | None :
    """Permet de charger le modèle se trouvant au chemin passé en paramètre."""
    
    importer : AbstractImporter = None
        
    with open(path) as file_data : 

        yaml_data = yaml.load(file_data, Loader = yaml.FullLoader)
        
        name = os.path.basename(path)
        
        path = path[:-name.__len__()]
        if name.endswith('.yaml') :
            name = name[:-('.yaml'.__len__())]

        if 'simulation' in yaml_data :
            importer = SimulationsImporter()
            data = yaml_data['simulation']

        if importer is None : 
            return None

        return importer.open(data, path, name)

def save (model : AbstractEditableModel, path : str) -> bool :
    """Permet de sauvegarder le modèle au chemin passé en paramètre."""
    
    exporter : AbstractExporter = None

    if isinstance(model, SimulationModel) :
        exporter = SimulationExporter()

    elif isinstance(model, BoxCompositeModel) :
        print('Save box composite !')
        raise Exception('no exporter found for box composite model')

    elif isinstance(model, SchedulerModel) :
        print('Save scheduler !')
        raise Exception('no exporter found for scheduler model')

    elif isinstance(model, BoxCodeModel) :
        print('Save box code !')
        raise Exception('no exporter found for box code model')

    return exporter.save(model, path)

def export (editor : AbstractEditor, argument : str) :
    """Permet d'exporter l'éditeur sous une forme voulue."""