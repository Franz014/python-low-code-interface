#-----------------------------------
# Imports
#-----------------------------------

from packages.core.plci_core_data_type import DataType
from packages.interface.graphical_actions.abstract_unique_element_diagram_action import AbstractUniqueElementDiagramAction
import packages.interface.models.color as c
from packages.interface.models.graphical_models.abstract_graphical_element import AbstractGraphicalElement

#-----------------------------------
# Class
#-----------------------------------

class DiagramAddOutputToBoxAction (AbstractUniqueElementDiagramAction) :
    """Est la classe d'action permettant d'ajouter une sortie à une box."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self, graphical_element : AbstractGraphicalElement, index : int, name : str, data_type : DataType, text : str = '', is_infinite : bool = True, color : c.Color = c.black) :

        AbstractUniqueElementDiagramAction.__init__(self, graphical_element)

        self._output_added_graphical_index = None
        self._output_index = index
        self._output_name = name
        self._output_data_type = data_type
        self._output_text = text
        self._output_is_infinite = is_infinite
        self._output_color = color

    # -------------
    # Methods
    # -------------

    def do (self) :
        
        output_model = self.graphical_element.insert_output(
            self._output_index, 
            self._output_name, 
            self._output_data_type, 
            self._output_text, 
            self._output_is_infinite,
            self._output_color)
        
        if self._output_added_graphical_index is None :
            self._output_added_graphical_index = output_model.graphical_index

        else :
            output_model.graphical_index = self._output_added_graphical_index


    def undo (self) :
        
        box = self.graphical_element

        if box is None : 
            return

        for output_index in range(box.output_len - 1, -1, -1) :

            output = box.get_output(output_index)
            if output.graphical_index == self._output_added_graphical_index :             
                box.remove_output(output)