#-----------------------------------
# Imports
#-----------------------------------

from typing import List, Dict

from packages.interface.graphical_actions.abstract_unique_element_diagram_action import AbstractUniqueElementDiagramAction
from packages.interface.models.graphical_models.abstract_box_model import AbstractBoxModel
from packages.interface.models.signals_model.input_connection_model import InputConnectionModel
from packages.interface.graphical_actions import parser_decode as parser_decode
from packages.interface.graphical_actions import parser_encode as parser_encode

#-----------------------------------
# Class
#-----------------------------------

class DiagramRemoveInputsFromBoxAction (AbstractUniqueElementDiagramAction) :
    """Est l'action permettant de supprimer les entrées d'une box."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self, box_element : AbstractBoxModel, inputs : List[InputConnectionModel]) :

        AbstractUniqueElementDiagramAction.__init__(self, box_element)

        self._reconstructors : List[Dict] = []

        for input in inputs : 

            input_dict : Dict = {}
            input_dict['graphical_index'] = input.graphical_index
            self._reconstructors.append(input_dict)


    # -------------
    # Methods
    # -------------

    def get_input (self, graphical_index : int, box_model : AbstractBoxModel) -> InputConnectionModel : 
        """Pemret de récuperer l'entrée de la box model suivant son graphical index."""

        for input in box_model.inputs :
            if input.graphical_index == graphical_index : 
                return input

        return None

    def do (self) :

        box_model : AbstractBoxModel = self.graphical_element

        if box_model is None : 
            return

        for input_dict in self._reconstructors :
            input = self.get_input(input_dict['graphical_index'], box_model)

            if input is None : 
                continue

            current_input : Dict = parser_encode.box_input(input, input_dict)
            current_input['signals'] = []

            for signal in input.links : 
                current_input['signals'].append(parser_encode.link(signal))
                signal.unbind()
                self.diagram_model.remove_element(signal)

            input._links.clear()
            
            box_model.remove_input(input)

 
    def undo (self) :
        
        box_model : AbstractBoxModel = self.graphical_element 

        if box_model is None : 
            return

        self._reconstructors.reverse()
        for input_dict in self._reconstructors :
            parser_decode.box_input(input_dict, box_model)

            for signal_dict in input_dict['signals'] : 
                parser_decode.link(signal_dict, self.diagram_model)