#-----------------------------------
# Imports
#-----------------------------------

import yaml

from typing import Any, Dict

from packages.interface.graphical_actions.abstract_diagram_action import AbstractDiagramAction
from packages.interface.models.graphical_models.abstract_box_model import AbstractBoxModel
from packages.interface.models.graphical_models.abstract_graphical_element import AbstractGraphicalElement
from packages.interface.models.links_model.link_type import get_link_type
import packages.interface.files.simulation_files.simulation_importer as importer
from packages.interface.models.signals_model.signal_link_model import SignalLinkModel

#-----------------------------------
# Class
#-----------------------------------

class DiagramPasteGraphicalElementAction (AbstractDiagramAction) :
    """Est l'action capable de coller le contenus d'une box."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self, diagram_editor, diagram_model : Any, content_paste : str) :
        
        AbstractDiagramAction.__init__(self, diagram_model)

        self._serialized_diagram = None
        self._diagram_editor = diagram_editor
        self._content_paste = content_paste
        self._elements_indices = []

    # -------------
    # Methods
    # -------------

    def set_or_get_graphical_index (self, age : AbstractGraphicalElement, dictionary : Dict) -> None :
        """Permet de récuperer ou de sélectionner le graphical index pour l'élément passé en paramètre."""
        
        if 'graphical_index' in dictionary : 
            age.graphical_index = dictionary['graphical_index']
        
        else :
            dictionary['graphical_index'] = age.graphical_index
            self._should_reload = True

            if isinstance(age, SignalLinkModel) or isinstance(age, AbstractBoxModel) : 
                if not(age.graphical_index in self._elements_indices) :
                    self._elements_indices.append(age.graphical_index)

    def do (self) :
        
        self._diagram_editor.unslect_elements()

        if self._serialized_diagram is None : 
            self._serialized_diagram = yaml.load(self._content_paste, yaml.FullLoader)

        if 'MuPhyN' in self._serialized_diagram :
            
            signals = self._serialized_diagram['MuPhyN']['signals']
            
            for signal in signals : 
                signal['input'] = None
                signal['output'] = None

            for box_dict in self._serialized_diagram['MuPhyN']['boxes'] :
                box_model = importer.construct_box(box_dict)
                self.set_or_get_graphical_index(box_model, box_dict)

                input_index = 0
                for input_dict in box_dict['inputs'] : 

                    input = importer.construct_input(input_dict, input_index, box_model)
                    self.set_or_get_graphical_index(input, input_dict)
                    
                    input_signal_index = int(input_dict['signal_index'])

                    if input_signal_index < len(signals) and input_signal_index >= 0 : 

                        signal_data = signals[input_signal_index]

                        if signal_data['input'] is None : 
                            
                            signal_data['input'] = input

                            if not(signal_data['output'] is None) : 

                                link = self._diagram_model.link_nodes(input, signal_data['output'], -1, -1,
                                    float(signal_data['link_value']), get_link_type(signal_data['link_type']), signal_data['text'])
                                self.set_or_get_graphical_index(link, signal_data)

                                link.setSelected(True)

                    input_index += 1

                output_index = 0
                for output_dict in box_dict['outputs'] : 
                    output = importer.construct_output(output_dict, output_index, box_model)
                    self.set_or_get_graphical_index(output, output_dict)
                
                    for output_signal_index in output_dict['signal_indices'] : 
                        if output_signal_index < len(signals) and output_signal_index >= 0 : 
                            
                            signal_data = signals[output_signal_index]

                            if signal_data['output'] is None : 

                                signal_data['output'] = output

                                if not(signal_data['input'] is None) : 
                                    
                                    link = self._diagram_model.link_nodes(signal_data['input'], output, -1, -1,
                                        float(signal_data['link_value']), get_link_type(signal_data['link_type']), signal_data['text'])
                                    self.set_or_get_graphical_index(link, signal_data)
                                    link.setSelected(True)

                    output_index += 1

                self._diagram_model.add_element(box_model)
                box_model.setSelected(True)

    def undo (self) :
        
        for element_index in self._elements_indices :

            element = self.diagram_model.get_element_by_graphical_index(element_index)

            if element is None : 
                continue

            if isinstance(element, SignalLinkModel) :
                element.unbind()
                self.diagram_model.remove_element(element)

        for element_index in self._elements_indices :

            element = self.diagram_model.get_element_by_graphical_index(element_index)

            if element is None : 
                continue

            if not(isinstance(element, SignalLinkModel)) :
                self.diagram_model.remove_element(element)