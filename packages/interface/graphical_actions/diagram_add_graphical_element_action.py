#-----------------------------------
# Imports
#-----------------------------------

from typing import List, Dict, Any
import copy
from PyQt5.QtCore import QPointF, QSizeF
from packages.core.box_library.box_library_data import AbstractBoxData
from packages.core.plci_core_data_type import get_data_type
from packages.interface.graphical_actions.abstract_diagram_action import AbstractDiagramAction
from packages.interface.models.graphical_models.box_model import BoxModel
from packages.interface.models.signals_model.abstract_signal_model import AbstractSignalModel

#-----------------------------------
# Functions
#-----------------------------------

def construct_box (box_data : AbstractBoxData, event_pos : QPointF, loadable : bool) -> BoxModel :
    box_model : BoxModel = BoxModel(box_data.box_library, box_data.box_name, event_pos, QSizeF(75, 75), 0, loadable)

    for input in box_data.inputs :
        is_infinite = input['count'] == 'infinite'
        if is_infinite and box_data._inputs.__len__() == 1 and not(box_data.box_name == 'Graph' and box_data.box_library == 'Boxes.Sinks') :
                box_model.insert_input(0, input['name'], get_data_type(input['type']), is_infinite = is_infinite)
                box_model.insert_input(1, input['name'], get_data_type(input['type']), is_infinite = is_infinite)
        
        else :
            box_model.insert_input(box_model.input_len, input['name'], get_data_type(input['type']), is_infinite = is_infinite)

    for output in box_data.outputs :
        is_infinite = output['count'] == 'infinite'
        box_model.insert_output(box_model.output_len, output['name'], get_data_type(output['type']), is_infinite = is_infinite)

    if hasattr(box_data, 'params') :
        for parameter in box_data.params :
            box_model._parameters[parameter] = copy.deepcopy(box_data.params[parameter])

    return box_model


#-----------------------------------
# Class
#-----------------------------------

class DiagramAddGraphicalElementAction (AbstractDiagramAction) :
    """Est le type d'action qui permet d'ajouter des éléments dans l'écran."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self, diagram_model : Any, constructors_ : List[Dict]) :
        
        AbstractDiagramAction.__init__(self, diagram_model)

        self._elements_indices = []
        self._constructors = constructors_
    
    # -------------
    # Methods
    # -------------

    def do (self) :
        
        for constructor in self._constructors :
            box_model = construct_box(constructor['box_data'], constructor['pos'], True)
            self.diagram_model.add_element(box_model)

            if 'graphical_index' in constructor :
                box_model.graphical_index = constructor['graphical_index']

                for index in range(len(constructor['inputs'])) :
                    box_model._inputs[index].graphical_index = constructor['inputs'][index]

                for index in range(len(constructor['outputs'])) :
                    box_model._outputs[index].graphical_index = constructor['outputs'][index]

            else :
                constructor['graphical_index'] = box_model.graphical_index
                constructor['inputs'] = []
                constructor['outputs'] = []

                for input in box_model.inputs : 
                    constructor['inputs'].append(input.graphical_index)

                for output in box_model.outputs : 
                    constructor['outputs'].append(output.graphical_index)

            self._elements_indices.append(box_model.graphical_index)

    def undo (self) :
        
        for graphical_element in self.diagram_model._graphical_elements :

            if graphical_element.graphical_index in self._elements_indices :

                if isinstance(graphical_element, AbstractSignalModel) :
                    graphical_element.unbind()

                self.diagram_model.remove_element(graphical_element)


            if len(self._elements_indices) == 0 : 
                break