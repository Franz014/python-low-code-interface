#-----------------------------------
# Imports
#-----------------------------------

import copy
from typing import Any, Dict

from packages.interface.models.graphical_models.abstract_box_model import AbstractBoxModel
from packages.interface.models.graphical_models.abstract_graphical_element import AbstractGraphicalElement
from packages.interface.models.graphical_models.box_input_model import BoxInputModel
from packages.interface.models.graphical_models.box_model import BoxModel
from packages.interface.models.graphical_models.box_output_model import BoxOutputModel
from packages.interface.models.links_model.abstract_link_model import AbstractLinkModel
from packages.interface.models.signals_model.input_connection_model import InputConnectionModel
import packages.interface.models.color as c

#-----------------------------------
# Functions
#-----------------------------------

def color (color : c.Color) -> Dict : 
    """Permet de créer le reconstructeur pour une coleur."""
    return {'red' : color.red, 'green' : color.green, 'blue' : color.blue}

def link (link : AbstractLinkModel) -> None :
    """Permet de peupler la liste de reconstructeur avec les éléments nécessaire pour reconstruire le lien actuel."""

    reconstructor = {}
    reconstructor['type'] = 'link'

    reconstructor['graphical_index'] = link.graphical_index
    reconstructor['input_box_index'] = link.input.parent().graphical_index
    reconstructor['output_box_index'] = link.output.parent().graphical_index
    reconstructor['input_index'] = link.input.graphical_index
    reconstructor['output_index'] = link.output.graphical_index

    reconstructor['link_type'] = link.link_type
    reconstructor['link_value'] = link.link_value
    reconstructor['link_text'] = link.text

    return copy.deepcopy(reconstructor)

def box (box : BoxModel) -> Dict : 
    """Permet de peupler la liste de reconstructeur avec les éléments nécessaire pour reconstruire une box."""

    reconstructor = abstract_box(box)
    reconstructor['type'] = 'box'
    reconstructor['library'] = box.library

    reconstructor['params'] = {}
    for param_name in box.get_parameters() :
        reconstructor['params'][param_name] = {}
        reconstructor['params'][param_name]['type'] = box.get_parameter(param_name)['type']
        reconstructor['params'][param_name]['value'] = box.get_parameter(param_name)['value']
    
    return copy.deepcopy(reconstructor)

def composite_box_input (input : BoxInputModel) -> Dict :
    """Permet de peupler la liste de reconstructeur avec les éléments nécesaire pour reconstruire une entrée de box composite."""

    reconstructor = abstract_box(input)
    reconstructor['type'] = 'box-composite-input'
    
    return copy.deepcopy(reconstructor)

def composite_box_output (output : BoxOutputModel) -> Dict :
    """Permet de peupler la liste de reconstructeur avec les éléments nécessaire pour reconstruire une sortie de box composite."""

    reconstructor = abstract_box(output)
    reconstructor['type'] = 'box-composite-output'
    
    return copy.deepcopy(reconstructor)

def box_input (input : InputConnectionModel, input_reconstructor : Dict = {}) -> Dict :
    """Permet de créer un dictionnaire contenant les données d'une entrée de box."""

    input_reconstructor['graphical_index'] = input.graphical_index
    input_reconstructor['name'] = input.name
    input_reconstructor['type'] = input.data_type
    input_reconstructor['is_infinite'] = input.is_infinite
    input_reconstructor['text'] = input.text
    input_reconstructor['color'] = color(input.color)
    input_reconstructor['index'] = input.parent().index_of_input(input)

    return copy.deepcopy(input_reconstructor)

def box_output (output : InputConnectionModel, output_reconstructor : Dict = {}) -> Dict :
    """Permet de créer un dictionnaire contenant les données d'une soirte de box."""
    
    output_reconstructor['graphical_index'] = output.graphical_index
    output_reconstructor['name'] = output.name
    output_reconstructor['type'] = output.data_type
    output_reconstructor['is_infinite'] = output.is_infinite
    output_reconstructor['text'] = output.text
    output_reconstructor['color'] = color(output.color)
    output_reconstructor['index'] = output.parent().index_of_output(output)

    return copy.deepcopy(output_reconstructor)

def abstract_box (abstract_box : AbstractBoxModel) -> Dict :
    """Permet de peupler un reconstructeur avec les éléments d'une box abstraite."""

    reconstructor = abstract_graphical_element(abstract_box)

    reconstructor['inputs'] = []
    for input in abstract_box.inputs : 
        reconstructor['inputs'].append(box_input(input))

    reconstructor['outputs'] = []
    for output in abstract_box.outputs : 
        reconstructor['outputs'].append(box_output(output))

    return copy.deepcopy(reconstructor)

def abstract_graphical_element (abstract_graphical_element : AbstractGraphicalElement) -> Dict :
    """Permet de peupler un reconstructeur avec les éléments d'un graphical element."""

    reconstructor = {}

    reconstructor['graphical_index'] = abstract_graphical_element.graphical_index
    reconstructor['geometry'] = {}
    reconstructor['geometry']['x'] = abstract_graphical_element.position.x()
    reconstructor['geometry']['y'] = abstract_graphical_element.position.y()
    reconstructor['geometry']['width'] = abstract_graphical_element.size.width()
    reconstructor['geometry']['height'] = abstract_graphical_element.size.height()
    reconstructor['geometry']['rotation'] = abstract_graphical_element.rot
    reconstructor['color'] = color(abstract_graphical_element.color)
    reconstructor['name'] = abstract_graphical_element.name
    reconstructor['text'] = abstract_graphical_element.text

    return copy.deepcopy(reconstructor)
    

def encode (element : Any) -> Dict :
    """Permet de sélectionne la bonne méthode à appeler pour peupler le reconstructeur."""

    if isinstance(element, AbstractLinkModel) :
        return link(element)

    elif isinstance(element, BoxModel) :
        return box(element)

    elif isinstance(element, BoxInputModel) :
        return composite_box_input(element)

    elif isinstance(element, BoxOutputModel) :
        return composite_box_output(element)