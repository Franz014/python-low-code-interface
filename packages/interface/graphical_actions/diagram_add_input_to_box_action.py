#-----------------------------------
# Imports
#-----------------------------------

from packages.core.plci_core_data_type import DataType
from packages.interface.graphical_actions.abstract_unique_element_diagram_action import AbstractUniqueElementDiagramAction
from packages.interface.models.graphical_models.abstract_graphical_element import AbstractGraphicalElement
import packages.interface.models.color as c

#-----------------------------------
# Class
#-----------------------------------

class DiagramAddInputToBoxAction (AbstractUniqueElementDiagramAction) :
    """Est la classe d'action permettant d'ajouter une entrée à une box."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self, graphical_element : AbstractGraphicalElement, index : int, name : str, data_type : DataType, text : str = '', is_infinite : bool = True, color : c.Color = c.black) :

        AbstractUniqueElementDiagramAction.__init__(self, graphical_element)

        self._input_added_graphical_index = None
        self._input_index = index
        self._input_name = name
        self._input_data_type = data_type
        self._input_text = text
        self._input_is_infinite = is_infinite
        self._input_color = color

    # -------------
    # Methods
    # -------------

    def do (self) :
        
        input_model = self.graphical_element.insert_input(self._input_index, self._input_name, self._input_data_type, self._input_text, self._input_is_infinite, self._input_color)
        
        if self._input_added_graphical_index is None :
            self._input_added_graphical_index = input_model.graphical_index

        else :
            input_model.graphical_index = self._input_added_graphical_index


    def undo (self) :
        
        box = self.graphical_element

        if box is None : 
            return

        for input_index in range(box.input_len - 1, -1, -1) :

            input = box.get_input(input_index)
            if input.graphical_index == self._input_added_graphical_index :             
                box.remove_input(input)