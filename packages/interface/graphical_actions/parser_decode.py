#-----------------------------------
# Imports
#-----------------------------------

from typing import Dict

from PyQt5.QtCore import QPointF, QSizeF

from packages.interface.models.graphical_models.abstract_box_model import AbstractBoxModel
from packages.interface.models.graphical_models.abstract_graphical_element import AbstractGraphicalElement
from packages.interface.models.graphical_models.box_input_model import BoxInputModel
from packages.interface.models.graphical_models.box_model import BoxModel
from packages.interface.models.graphical_models.box_output_model import BoxOutputModel
from packages.interface.models.signals_model.input_connection_model import InputConnectionModel
from packages.interface.models.signals_model.output_connection_model import OutputConnectionModel
from packages.interface.models.signals_model.signal_link_model import SignalLinkModel
from packages.interface.models.editable_models.abstract_diagram_model import AbstractDiagramModel
import packages.interface.models.color as c

#-----------------------------------
# Functions
#-----------------------------------

def color (color_dict : Dict) -> c.Color :
    """Permet de reconstruire une couleur sur base d'un dictionnaire."""
    return c.Color(color_dict['red'], color_dict['green'], color_dict['blue'])
    
def link (link_reconstructor : Dict, diagram_model : AbstractDiagramModel) -> SignalLinkModel :
    """Permet de reconstruire un lien suivant son reconstructeur."""
    
    input_box = diagram_model.get_element_by_graphical_index(link_reconstructor['input_box_index'])
    output_box = diagram_model.get_element_by_graphical_index(link_reconstructor['output_box_index'])

    if input_box is None : 
        print('reconstruct link : input box = None')
        return None

    if not(hasattr(input_box, '_inputs')) :
        print('reconstruct link : input box has no inputs')
        return None

    if output_box is None : 
        print('reconstruct link : output box = None')
        return None

    if not(hasattr(output_box, '_outputs')) :
        print('reconstruct link : output box has no inputs')
        return None

    input = None
    output = None

    for i in input_box.inputs : 
        if i.graphical_index == link_reconstructor['input_index'] :
            input = i
            break

    for o in output_box.outputs :
        if o.graphical_index == link_reconstructor['output_index'] : 
            output = o
            break

    if input is None :
        print('reconstruct link : no input found')
        return None
        
    if output is None : 
        print('reconstruct link : no output found')
        return None


    link_model = diagram_model.link_nodes(input, output, link_value = link_reconstructor['link_value'], link_type = link_reconstructor['link_type'], link_text = link_reconstructor['link_text']) 
    link_model.graphical_index = link_reconstructor['graphical_index']

    return link_model

def box (box_reconstructor : Dict) -> BoxModel :
    """Permet de reconstruire une box suivant son reconstructeur."""

    box_model = BoxModel(
        box_reconstructor['library'], 
        box_reconstructor['name'], 
        QPointF(box_reconstructor['geometry']['x'], box_reconstructor['geometry']['y']), 
        QSizeF(box_reconstructor['geometry']['width'], box_reconstructor['geometry']['height']),
        box_reconstructor['geometry']['rotation'],
        True,
        box_reconstructor['text'] 
    )

    for param_name in box_reconstructor['params'] :
        box_model.create_parameter(param_name, box_reconstructor['params'][param_name]['type'], box_reconstructor['params'][param_name]['value'])

    abstract_box_model(box_model, box_reconstructor)

    return box_model

def composite_box_input (input_reconstructor : Dict) -> BoxInputModel :
    """Permet de reconstruire une entrée de box composite suivant son reconstructeur."""
    raise Exception('No reconstructor created yet for the composite box input !!!')

def composite_box_output (output_reconstructor : Dict) -> BoxOutputModel :
    """Permet de reconstruire une sortie de box composite suivant son reconstructeur."""
    raise Exception('No reconstructor created yet for the composite box output !!!')

def box_input (input_dict : Dict, box_model : AbstractBoxModel) -> InputConnectionModel :
    """Permet de reconstruire une entrée de box suivant son reconstructeur."""
    input = box_model.insert_input(
            input_dict['index'],
            input_dict['name'],
            input_dict['type'],
            input_dict['text'],
            input_dict['is_infinite'],
            color(input_dict['color'])
            )
    input.graphical_index = input_dict['graphical_index']

    return input


def box_output (output_dict : Dict, box_model : AbstractBoxModel) -> OutputConnectionModel :
    """Permet de reconstruire une sortie de box suivant son reconstructeur."""
    output = box_model.insert_output(
            output_dict['index'],
            output_dict['name'],
            output_dict['type'],
            output_dict['text'],
            output_dict['is_infinite'],
            color(output_dict['color'])
            )
    output.graphical_index = output_dict['graphical_index']

    return output

def abstract_box_model (box : AbstractBoxModel, box_reconstructor : Dict) -> AbstractBoxModel :
    """Permet de repeupler les données contenues dans le reconstructeur dans l'abstract box model."""

    for input_dict in box_reconstructor['inputs'] :
        box_input(input_dict, box)

    for output_dict in box_reconstructor['outputs'] :
        box_output(output_dict, box)

    abstract_graphical_element(box, box_reconstructor)
    return box

def abstract_graphical_element (graphical_element : AbstractGraphicalElement, element_reconstructor : Dict) -> AbstractGraphicalElement :
    """Permet de repeupler les données contenues dans le reconstructeur dans l'objet graphique."""
    graphical_element.color = color(element_reconstructor['color'])
    graphical_element.graphical_index = element_reconstructor['graphical_index']
    return graphical_element

def decode (element_reconstructor : Dict) -> AbstractGraphicalElement :
    """Permet de retourner un l'élément contenu dans le dictionnaire."""

    if element_reconstructor is None :
        return

    if element_reconstructor['type'] == 'box' :
        return box(element_reconstructor)

    elif element_reconstructor['type'] == 'box-composite-input' :
        return composite_box_input(element_reconstructor)

    elif element_reconstructor['type'] == 'box-composite-output' :
        return composite_box_output(element_reconstructor)
