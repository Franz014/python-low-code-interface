#-----------------------------------
# Imports
#-----------------------------------

from typing import Iterable
from PyQt5 import QtGui
from PyQt5 import QtCore

from PyQt5.QtCore import QMimeData, QPointF, QRectF, QSizeF, Qt
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsSceneHoverEvent, QGraphicsSceneMouseEvent, QGraphicsTextItem, QStyleOptionGraphicsItem
from packages.core.plci_core_data import Data
from packages.core.plci_core_data_type import DataType
from packages.interface.models.signals_model.abstract_connection_model import AbstractConnectionModel, static_io_size, static_io_size_2, static_io_path
from packages.interface.models.signals_model.signal_link_model import SignalLinkModel
import packages.interface.models.color as c
from packages.interface.models.signals_model.signal_node_model import SignalNodeModel, start_drag

#-----------------------------------
# Class
#-----------------------------------

class OutputConnectionModel (AbstractConnectionModel) :
    """Est la classe décrivant le fonctionnement des sorties des boxes."""
    
    # -------------
    # Constructors
    # -------------

    def __init__ (self, name : str, data_type : DataType, position : QPointF, link : SignalLinkModel = None,
                  default_value : Data = None, text : str = '', is_infinite : bool = False, parent : QGraphicsItem = None) :
        
        links : Iterable[SignalLinkModel] = []
        if not(link is None) :
            links.append(link)

        AbstractConnectionModel.__init__(self, name, data_type, position, static_io_size, links, text, is_infinite, parent)

        self._default_value : Data = default_value

        self.setFlag(QGraphicsItem.ItemIsMovable, False)
        self._text_pos = None
        self._lbl = None

    # -------------
    # Properties
    # -------------

    @property
    def default_value (self) -> Data :
        """Permet de récuperer la valeur par défaut du signal."""
        return self._default_value


    @default_value.setter
    def default_value (self, default_value_ : Data) -> None :
        """Permet de modifier la valeur par défaut du signal."""
        self._default_value = default_value_

    @property
    def is_connected_to_input (self) -> bool :
        """Permet de savoir si l'élément actuel est connecté à une entrée (ou est un entrée)."""
        return False

    # -------------
    # Methods
    # -------------

    def mousePressEvent (self, event: QGraphicsSceneMouseEvent) -> None :
        if (event.button() == Qt.LeftButton and self.isUnderMouse()) :
            start_drag(self, event)
            event.accept() 

        return super().mousePressEvent(event)

    def paint (self, painter : QtGui.QPainter, option: QStyleOptionGraphicsItem, widget) -> None :
        
        painter.fillRect(int(-static_io_size_2.width() - 1), int(-static_io_size_2.height()), int(self._bound.width() + 2), int(self._bound.height()), self.white_brush)
        painter.strokePath(static_io_path, self._path_pen)

        if self._text_pos == None : 
            self._font_metrics_used = QtGui.QFontMetrics(painter.font())
            self._text_pos = QPointF(static_io_size.width(), (- self._font_metrics_used.height()) / 2) 
            

    def hoverEnterEvent(self, event : QGraphicsSceneHoverEvent) -> None :
        if self._lbl == None : 
            self._lbl = QGraphicsTextItem(self)
            self._lbl.setPlainText(self.rendered_text)
            self._lbl.setPos(self._text_pos) 
        
        self._lbl.setVisible(True)
        return super().hoverEnterEvent(event)

    def hoverLeaveEvent(self, event : QGraphicsSceneHoverEvent) -> None :
        self._lbl.setVisible(False)
        return super().hoverLeaveEvent(event)