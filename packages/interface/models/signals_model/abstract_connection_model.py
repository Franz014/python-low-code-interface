#-----------------------------------
# Imports
#-----------------------------------

from typing import Iterable

from PyQt5 import QtGui
from PyQt5.QtCore import QPointF, QSizeF, Qt
from PyQt5.QtWidgets import QGraphicsItem

from packages.core.plci_core_data_type import DataType
from packages.interface.models.links_model.abstract_node_model import AbstractNodeModel
from packages.interface.models.signals_model.abstract_signal_model import AbstractSignalModel
from packages.interface.models.signals_model.signal_link_model import SignalLinkModel

#-----------------------------------
# Class
#-----------------------------------

static_io_size = QSizeF(10, 22)
static_io_size_2 = QSizeF(static_io_size.width() / 2, static_io_size.height() / 2)
static_io_path = QtGui.QPainterPath(QPointF(-static_io_size_2.width(), -static_io_size_2.height()))

static_io_path.lineTo(-static_io_size_2.width(), (-static_io_size_2.height()) + 1)
static_io_path.lineTo(static_io_size_2.width(), 0)
static_io_path.lineTo(-static_io_size_2.width(), static_io_size_2.height() + 1)
static_io_path.lineTo(-static_io_size_2.width(), static_io_size_2.height())

class AbstractConnectionModel (AbstractSignalModel, AbstractNodeModel[AbstractSignalModel]) : 
    """Est la classe abstraite communes aux models représentant des connexions entre les boxes et les signaux."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self, name : str, data_type : DataType, position : QPointF, size : QSizeF, links : Iterable[SignalLinkModel] = [],
                  text : str = '', is_infinite : bool = False, parent : QGraphicsItem = None) :
        
        AbstractSignalModel.__init__(self, data_type)
        AbstractNodeModel.__init__(self, name, position, size, links, text, parent)

        self._is_infinite = is_infinite

    # -------------
    # Properties
    # -------------
        
    @property 
    def is_connected (self) -> bool :
        """Permet de savoir si la connexion est bien connecter à un signal."""
        return self._links.__len__() > 0

    @property
    def is_infinite (self) -> bool :
        """Permet de savoir si la connexion est ou fait partit d'un nombre infinie de connexion."""
        return self._is_infinite

    @property
    def diagram_model (self) :
        """Permet de récuperer l'éditeur visuel."""
        return self.parent().diagram_model