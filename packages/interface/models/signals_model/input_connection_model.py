#-----------------------------------
# Imports
#-----------------------------------

import ctypes
from typing import Any

from PyQt5 import QtGui
from PyQt5.QtCore import QPointF, Qt

from PyQt5.QtCore import QPointF
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsSceneDragDropEvent, QGraphicsSceneHoverEvent, QGraphicsTextItem, QStyleOptionGraphicsItem
from packages.core.plci_core_data_type import DataType
from packages.interface.graphical_actions.diagram_link_nodes_action import DiagramLinkNodesAction
from packages.interface.models.signals_model.signal_node_model import SignalNodeModel
from packages.interface.models.signals_model.abstract_connection_model import static_io_size, static_io_size_2, static_io_path

#-----------------------------------
# Class
#-----------------------------------

class InputConnectionModel (SignalNodeModel) :
    """Est la classe décrivant le fonctionnement des entrées des boxes."""
    
    # -------------
    # Constructors
    # -------------

    def __init__ (self, name : str, data_type : DataType, position : QPointF, text : str = '', is_infinite : bool = False, 
                  parent : QGraphicsItem = None) :

        SignalNodeModel.__init__(self, name, data_type, position, static_io_size, [], text, is_infinite, parent)
        
        self.setFlag(QGraphicsItem.ItemIsMovable, False)
        self.setAcceptDrops(True)

        self._text_pos = None
        self._lbl = None

    # -------------
    # Properties
    # -------------

    @property
    def is_connected_to_input (self) -> bool :
        """Permet de savoir si l'élément actuel est connecté à une entrée (ou est un entrée)."""
        return True

    @property 
    def is_input (self) -> bool :
        """Permet de savoir si l'élément est une entrée."""
        return True

    # -------------
    # Methods
    # -------------

    def paint (self, painter : QtGui.QPainter, option: QStyleOptionGraphicsItem, widget) -> None :
        
        painter.fillRect(int(-static_io_size_2.width() - 1), int(-static_io_size_2.height()), int(self._bound.width() + 2), int(self._bound.height()), self.white_brush)
        painter.strokePath(static_io_path, self._path_pen)

        if self.is_connected :
            painter.fillPath(static_io_path, self.black_brush)
        
        if self._text_pos == None : 
            self._font_metrics_used = QtGui.QFontMetrics(painter.font())
            self._text_pos = QPointF(- (static_io_size.width() * 2 + self._font_metrics_used.width(self.rendered_text)), (- self._font_metrics_used.height()) / 2) 

    def dragEnterEvent (self, event: QGraphicsSceneDragDropEvent) -> None :
        
        if event.possibleActions() == Qt.LinkAction :
            if not(self.is_connected) :
                if self.data_type.__str__() == event.mimeData().data('type') :
                    event.accept()
                    return super().dragEnterEvent(event)
            
        event.ignore()

        return super().dragEnterEvent(event)

    def dragMoveEvent(self, event : QGraphicsSceneDragDropEvent) -> None :
        
        if event.possibleActions() == Qt.LinkAction :
            if not(self.is_connected) :
                if self.data_type.__str__() == event.mimeData().data('type') :
                    event.accept()
                    return super().dragEnterEvent(event)
        
        event.ignore()

        return super().dragEnterEvent(event)

    def dropEvent (self, event: QGraphicsSceneDragDropEvent) -> None :

        if event.possibleActions() == Qt.LinkAction and not(self.is_connected) :
            if event.mimeData().data('action') == 'new link' :
                node = ctypes.cast(int(event.mimeData().data('link')), ctypes.py_object).value
                action = DiagramLinkNodesAction(self, node)
                action.do()
                self.scene().parent().parent().actions_holder.append(action)
        
                event.accept()

        return super().dropEvent(event)

    def hoverEnterEvent(self, event : QGraphicsSceneHoverEvent) -> None :
        if self._lbl == None : 
            self._lbl = QGraphicsTextItem(self)
            self._lbl.setPlainText(self.rendered_text)
            self._lbl.setPos(self._text_pos)
        
        self._lbl.setVisible(True)
        return super().hoverEnterEvent(event)

    def hoverLeaveEvent(self, event : QGraphicsSceneHoverEvent) -> None :
        self._lbl.setVisible(False)
        return super().hoverLeaveEvent(event)

    def add_link (self, link : Any) -> None :
        """Permet d'ajouter un lien à la position données."""

        if link is None : 
            return

        if len(self._links) > 0 :
            return

        self._links.append(link)

    def insert_link (self, index : int, link : Any) -> None :
        """Permet d'insérer un lien à la position données."""

        if link is None : 
            return

        if len(self._links) > 0 :
            return

        if index > len(self._links) :
            return

        self._links.insert(index, link)