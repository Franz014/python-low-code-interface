#-----------------------------------
# Imports
#-----------------------------------

from typing import Iterable, List, Any

from PyQt5 import QtGui
from PyQt5.QtCore import QPointF, QSize, QSizeF, Qt, pyqtSignal
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsSceneMouseEvent, QStyleOptionGraphicsItem

from packages.core.plci_core_data_type import DataType
from packages.interface.models.event_signal_data import EventSignalData
from packages.interface.models.graphical_models.abstract_graphical_element import AbstractGraphicalElement
from packages.interface.models.signals_model.abstract_connection_model import static_io_size, static_io_size_2
from packages.interface.models.signals_model.input_connection_model import InputConnectionModel
from packages.interface.models.signals_model.output_connection_model import OutputConnectionModel
from packages.interface.models.graphical_models.resizers.horizontal_resizer import HorizontalResizer 
from packages.interface.models.graphical_models.resizers.vertical_resizer import VerticalResizer
from packages.interface.models.graphical_models.resizers.oblique_resizer import ObliqueResizer
from packages.interface.models.graphical_models.abstract_moveable_graphical_element import AbstractMoveableGraphicalElement
from packages.interface.models.signals_model.signal_link_model import SignalLinkModel
import packages.interface.models.color as c

#-----------------------------------
# Class
#-----------------------------------

io_margin : int = 4
name_margin : QPointF = QPointF(10, 5)

class AbstractBoxModel (AbstractMoveableGraphicalElement) :
    """Est la classe abstraite des éléments capable d'être rendu sous forme de boxes et affichés à l'écran."""

    # -------------
    # Signals
    # -------------
    
    input_count_changed = pyqtSignal()
    output_count_changed = pyqtSignal()

    # -------------
    # Constructors
    # -------------

    def __init__ (self, name : str, position : QPointF, size : QSizeF, rotation : float = 0.0, text : str = '', parent : QGraphicsItem = None) :
        
        self._font_metrics_used = None

        AbstractMoveableGraphicalElement.__init__(self, name, position, size, rotation, text, parent)

        self._inputs : List[InputConnectionModel] = []
        self._outputs : List[OutputConnectionModel]  = []

        self._should_recompute = False

        self._size_text : QSize = QSize(0, 0)

        self.resizer_oblique = ObliqueResizer(self)
        self.resizer_oblique.resizeSignal.connect(self.resize)
        self.resizer_oblique.selectedSignal.connect(self.selectionChanged)
        self.resizer_oblique.setVisible(False)

        self.resizer_horizontal = HorizontalResizer(self)
        self.resizer_horizontal.resizeSignal.connect(self.resize)
        self.resizer_horizontal.selectedSignal.connect(self.selectionChanged)
        self.resizer_horizontal.setVisible(False)

        self.resizer_vertical = VerticalResizer(self)
        self.resizer_vertical.resizeSignal.connect(self.resize)
        self.resizer_vertical.selectedSignal.connect(self.selectionChanged)
        self.resizer_vertical.setVisible(False)

        self.resize(EventSignalData(self, QPointF(0, 0)))

    # -------------
    # Properties
    # -------------
    
    @property
    def inputs (self) -> Iterable[InputConnectionModel] :
        """Permet de récuperer les signaux d'entrées."""

        for input in self._inputs :
            yield input

    @property
    def input_len (self) -> int :
        """Permet de récuperer le nombre d'entrées."""

        return self._inputs.__len__()

    @property
    def outputs (self) -> Iterable[OutputConnectionModel] :
        """Permet de récuperer les signaux de sorties."""

        for output in self._outputs :
            yield output

    @property
    def output_len (self) -> int :
        """Permet de récuperer le nombre de sorties."""

        return self._outputs.__len__()

    @AbstractGraphicalElement.text.setter
    def text (self, text_ : str) -> None :

        AbstractGraphicalElement.text = text_
        self.recompute_name_position()
    
    @property
    def minimum_size (self) -> QPointF :
        """Permet de récuperer la taille minimum de la boxes."""
        
        minimum : QPointF = QPointF(0, 0)

        if not(self._font_metrics_used is None) :

            if self.rotation() == 0 or self.rotation() == 180 :
                ...

            else :
                ...

        return minimum

    @property
    def signals (self) -> Iterable[SignalLinkModel] :
        """Permet de récueperer la liste des liens connectés à la box."""

        _buff_already_passed = []

        for input in self._inputs :
            for signal in input._links : 
                if signal in _buff_already_passed :
                    continue

                yield signal
                _buff_already_passed.append(signal)

        for output in self._outputs :
            for signal in output._links : 
                if signal in _buff_already_passed :
                    continue

                yield signal
                _buff_already_passed.append(signal)

    # -------------
    # Methods
    # -------------

    def setRotation (self, angle: float) -> None :

        while angle < 0 :
            angle += 360

        while angle >= 360 :
            angle -= 360

        if angle < 45 :
            super().setRotation(0)

        elif angle < 135 :
            super().setRotation(90)

        elif angle < 225 :
            super().setRotation(180)

        elif angle < 315 :
            super().setRotation(270)

        else :
            super().setRotation(0)

    def recompute_name_position (self) -> None :
        """Permet de recalculer la taille du texte affiché et le recentrer dans la box."""
        
        if self._font_metrics_used is None :
            return

        self._size_text = QSize(int(self._font_metrics_used.width(self.rendered_text)), int(self._font_metrics_used.height()))
        
    def recompute_inputs_positions (self) -> None :
        """Permet de recalculer les positions des entrées."""

        if not(self._should_recompute) :
            return
        
        if self.input_len == 0 :
            return
        
        total_margin = self.size.height() - (self.input_len * static_io_size.height())
        margin = total_margin / (self.input_len + 1)

        for i in range(self.input_len) :
            self._inputs[i].setPos((-self._bound.width() / 2) + static_io_size_2.width(), margin + (i * (margin + static_io_size.height())) - self._bound.height() / 2 + static_io_size_2.height())

    def recompute_ouputs_positions (self) -> None :
        """Permet de recalculer les positions des sorties."""

        if not(self._should_recompute) :
            return
        
        if self.output_len == 0 :
            return

        total_margin = self.size.height() - (self.output_len * static_io_size.height())
        margin = total_margin / (self.output_len + 1)
        
        for i in range(self.output_len) :
            self._outputs[i].setPos(self.size.width() / 2 + static_io_size_2.width(), margin + (i * (margin + static_io_size.height())) - self._bound.height() / 2 + static_io_size_2.height())
 
    
    def insert_input (self, index : int, name : str, data_type : DataType, text : str = '', is_infinite : bool = False, color : c.Color = c.black) -> InputConnectionModel :
        """Permet d'insérer une entrée dans la box."""

        input = InputConnectionModel(name, data_type, QPointF(0, 2), text, is_infinite, self)
        input.color = color
        self._inputs.insert(index, input)
        self.recompute_inputs_positions()

        self.input_count_changed.emit()
        
        return input
        
    def get_input (self, index : int) -> InputConnectionModel : 
        """Permet de récuperer une entrée pour un index donné."""

        if index < 0 :
            return None

        if index >= self.input_len : 
            return None

        return self._inputs[index]

    def index_of_input (self, input : InputConnectionModel) -> int : 
        """Permet de connaire l'index de l'entrée passé en paramètre. Si elle n'est pas présente, retourne -1."""
        
        if input is None : 
            return -1

        if input in self._inputs :
            self._inputs.index(input)

        return -1

    def remove_input (self, input : InputConnectionModel) -> None :
        """Permet de suppprimer une entrée de la box."""

        for i in range(len(input._links)) :
            self.diagram_model.remove_element(input._links[i])
        
        self._inputs.remove(input)
        self.children().remove(input)
        input.deleteLater()

        self.recompute_inputs_positions()
        self.input_count_changed.emit()

    def insert_output (self, index : int, name : str, data_type : DataType, text : str = '', is_infinite : bool = False, color : c.Color = c.black) -> OutputConnectionModel :
        """Permet d'insérer une sortie dans la box."""
        
        output = OutputConnectionModel(name, data_type, QPointF(0, 2), None, None, text, is_infinite, self)
        output.color = color
        self._outputs.insert(index, output)
        self.recompute_ouputs_positions()

        self.output_count_changed.emit()
        
        return output
        
    def get_output (self, index : int) -> OutputConnectionModel : 
        """Permet de récuperer une entrée pour un index donné."""

        if index < 0 :
            return None

        if index >= self.output_len : 
            return None

        return self._outputs[index]

    def index_of_output (self, output : OutputConnectionModel) -> int : 
        """Permet de connaire l'index de la sortie passé en paramètre. Si elle n'est pas présente, retourne -1."""
        
        if output is None : 
            return -1

        if output in self._outputs :
            self._outputs.index(output)

        return -1

    def remove_output (self, output : OutputConnectionModel) -> None : 
        """Permet de supprimer une sortie de la box."""

        for i in range(len(output._links)) :
            self.diagram_model.remove_element(output._links[i])

        self._outputs.remove(output)
        self.children().remove(output)
        output.deleteLater()

        self.recompute_ouputs_positions()
        self.output_count_changed.emit()

    def select_pen (self) -> QtGui.QPen : 

        if self.isSelected() :
            return self.selected_black_pen
        
        return self.unselected_black_pen
        
    def resize (self, param : EventSignalData) -> None :
        
        super().resize(param)

        if not(param.sender == self.resizer_oblique) :
            resizersOffset = QPointF(self.resizer_oblique.boundingRect().width() / 2, self.resizer_oblique.boundingRect().height() / 2)
            self.resizer_oblique.setPos(QPointF(self.size.width() / 2 - resizersOffset.x(), self.size.height() / 2 - resizersOffset.y()))

        if not(param.sender == self.resizer_horizontal) :
            x = self._bound.bottomRight().x() - (self.resizer_horizontal.boundingRect().width() / 2)
            y = - self.resizer_horizontal.boundingRect().height() / 2
            self.resizer_horizontal.setPos(QPointF(x, y))

        if not(param.sender == self.resizer_vertical) :
            x = - self.resizer_vertical.boundingRect().width() / 2
            y = self._bound.bottomRight().y() - (self.resizer_vertical.boundingRect().height() / 2)
            self.resizer_vertical.setPos(QPointF(x, y))

        self.recompute_inputs_positions()
        self.recompute_ouputs_positions()
        self.recompute_name_position()
        self.size_changed_method()
    
    def paint (self, painter: QtGui.QPainter, option: QStyleOptionGraphicsItem, widget) -> None :
        
        if not(self._should_recompute) :
            self._should_recompute = True
            self.recompute_inputs_positions()
            self.recompute_ouputs_positions()

        self._font_metrics_used = QtGui.QFontMetrics(painter.font())
        self.recompute_name_position()

        painter.setBrush(self.white_brush)
        painter.setPen(self.select_pen())
        self.print_box(painter, option, widget)
        
    def print_text (self, painter: QtGui.QPainter, option: QStyleOptionGraphicsItem, widget) -> None :

        painter.rotate(-self.rotation())

        painter.drawText(int(-(self._size_text.width() / 2)), int(self._size_text.height() / 4), self.rendered_text)

        painter.rotate(self.rotation())

    def print_box (self, painter: QtGui.QPainter, option: QStyleOptionGraphicsItem, widget) -> None :

        painter.fillRect(self._bound, self.white_brush)
        painter.drawRect(self._bound)
        self.print_text(painter, option, widget)

    def mouseMoveEvent (self, event : QGraphicsSceneMouseEvent) -> None :
        
        if event.button() == Qt.LeftButton : 
            if self.resizer_horizontal.isUnderMouse() and self.resizer_oblique.isUnderMouse() and self.resizer_vertical.isUnderMouse() :
                event.accept()

        return super().mouseMoveEvent(event)

    def mousePressEvent (self, event : QGraphicsSceneMouseEvent) -> None :

        if event.button() == Qt.LeftButton : 
            if self.resizer_horizontal.isUnderMouse() and self.resizer_oblique.isUnderMouse() and self.resizer_vertical.isUnderMouse() :
                event.accept()

        return super().mousePressEvent(event)

    def mouseReleaseEvent(self, event: QGraphicsSceneMouseEvent) -> None :
        
        if event.button() == Qt.LeftButton : 
            if self.resizer_horizontal.isUnderMouse() and self.resizer_oblique.isUnderMouse() and self.resizer_vertical.isUnderMouse() :
                event.accept()

        return super().mouseReleaseEvent(event)
    
    def itemChange (self, change: QGraphicsItem.GraphicsItemChange, value: Any) -> Any :
        
        if change == QGraphicsItem.ItemSelectedChange :
            
            self.selectionChanged(EventSignalData(self, value))

        return super().itemChange(change, value)

    def selectionChanged (self, param : EventSignalData) -> None :

        if param.value == 1 :

            if param.sender == self or param.sender == self.resizer_oblique or param.sender == self.resizer_vertical or param.sender == self.resizer_horizontal :
                self.resizer_oblique.setVisible(True)
                self.resizer_horizontal.setVisible(True)
                self.resizer_vertical.setVisible(True)
                self.setZValue(2)
                return
        
        elif param.sender == self and (self.resizer_oblique.isUnderMouse() or self.resizer_vertical.isUnderMouse() or self.resizer_horizontal.isUnderMouse()) :
                self.resizer_oblique.setVisible(True)
                self.resizer_horizontal.setVisible(True)
                self.resizer_vertical.setVisible(True)
                self.setZValue(2)
                return

        self.resizer_oblique.setVisible(False)
        self.resizer_horizontal.setVisible(False)
        self.resizer_vertical.setVisible(False)
        self.setZValue(1)