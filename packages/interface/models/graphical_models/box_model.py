#-----------------------------------
# Imports
#-----------------------------------

from typing import Iterable, Any, Dict

from PyQt5 import QtGui
from PyQt5.QtCore import QPointF, QSizeF
from PyQt5.QtWidgets import QStyleOptionGraphicsItem

from packages.core.plci_core_data_type import DataType
from packages.interface.models.graphical_models.abstract_box_model import AbstractBoxModel

#-----------------------------------
# Class
#-----------------------------------

class BoxModel (AbstractBoxModel) :
    """Est le model graphique des boxes affichées à l'écran."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self, library : str, name : str, position : QPointF, size : QSizeF, rotation : float, 
                  can_be_loaded : bool = True, text : str = '') :
        
        self._library = library
        self._parameters : Dict[str, Any]= {}

        AbstractBoxModel.__init__(self, name, position, size, rotation, text, None)
        
        self._can_be_loaded : bool = can_be_loaded
        self.setZValue(1)

    # -------------
    # Properties
    # -------------

    @property
    def library (self) -> str : 
        """Permet de récuperer le nom de la bibliothèque dans laquelle se trouve la box."""
        return self._library

    @library.setter
    def library (self, library_ : str) -> None :
        """Permet de modifier le nom de la bibliothèque dans laquelle se trouve la box."""
        self._library = library_ or ''

    @property
    def can_be_loaded (self) -> bool :
        """Permet de savoir si la box actuelle est bien présente dans la bibliothèques."""
        return self._can_be_loaded

    @can_be_loaded.setter
    def can_be_loaded (self, can_be_loaded_ : bool) -> None : 
        """Permet de modifier le fait que la box actuelle est bien présente dans la bibliothèque."""
        self._can_be_loaded : bool = can_be_loaded_

    @property
    def rendered_text (self) -> str :
        return super().rendered_text

    @rendered_text.setter
    def rendered_text(self, rendered_text_ : str) -> None :

        self._text = rendered_text_

    # -------------
    # Methods
    # -------------

    def set_parameter (self, name : str, value : object) -> None :
        """Permet de modifier la valeur du paramètre.""" 

        old_value = self._parameters[name]['value']
        if value == old_value :
            return

        self._parameters[name]['value'] = value

        if self._action_param_semaphore == True :
            return
            
        self.param_changed.emit(self, 'params.'+name, old_value, value)
    
    def get_parameter (self, name : str) -> Dict :
        """Permet de récuperer les données d'un paramètre."""
        return self._parameters[name]

    def get_parameters (self) -> Iterable[str] :
        """Permet de récuperer la liste des paramètres."""
        for key in self._parameters :
            yield key

    def get_parameters_len (self) -> int :
        """Permet de récuperer la taille du tableau de paramètres."""
        return self._parameters.__len__()

    def create_parameter(self, parameter_name : str, parameter_type : DataType, parameter_value : object) -> None :
        """Permet de créer un slot pour le paramètre voulu."""
        self._parameters[parameter_name] = {}
        self._parameters[parameter_name]['type'] = parameter_type
        self._parameters[parameter_name]['value'] = parameter_value

    def recompute_name_position (self) -> None :

        if self._library == 'Boxes.Math' :
            if self._name == 'Addition' :
                self.rendered_text = '+'

            elif self._name == 'Amplifier' :
                if 'gain' in self._parameters :
                    self.rendered_text = str(self._parameters['gain']['value'])

                else :
                    self.rendered_text = '1'
            
            elif self._name == 'Multiplier' :
                self.rendered_text = 'X'
            
        elif self._library == 'Boxes.Sources' :
            if self._name == 'Constant' :
                if 'value' in self._parameters :
                    self.rendered_text = str(self._parameters['value']['value'])
                else :
                    self.rendered_text = '1'

        super().recompute_name_position()
    
    def select_pen (self) -> QtGui.QPen : 

        if self.isSelected() :
            if self._can_be_loaded :
                return self.selected_black_pen
            else :
                return self.selected_red_pen
        else :
            if self._can_be_loaded :
                return self.unselected_black_pen
            else :
                return self.unselected_red_pen

    def print_box (self, painter: QtGui.QPainter, option: QStyleOptionGraphicsItem, widget) -> None :

        self.recompute_name_position()

        if self.library == 'Boxes.Math' :
                   
            if self._name == 'Amplifier' :

                path = QtGui.QPainterPath(QPointF(-self.size.width() / 2, -self.size.height() / 2))
                path.lineTo(self.size.width() / 2, 0)
                path.lineTo(-self.size.width() / 2, self.size.height() / 2)
                path.lineTo(-self.size.width() / 2, -self.size.height() / 2)

                painter.fillPath(path, self.white_brush)
                painter.drawPath(path)

                self.print_text(painter, option, widget)
                return
        
        elif self.library == 'Boxes.Sources' :
            
            width_4 = self.size.width() / 4
            height_4 = self.size.height() / 4
            painter.drawEllipse(QPointF(0, 0), self.size.width() / 2, self.size.height() / 2)

            painter.setPen(self.unselected_black_pen)
            if self._name == 'Ramp' :

                if 'slope' in self._parameters :
                    
                    slope = self._parameters['slope']['value'] 
                    if slope < 0 :
                        path = QtGui.QPainterPath(QPointF(-width_4, -height_4))
                        path.lineTo(0, -height_4)
                        path.lineTo(width_4, height_4)

                    elif slope == 0 :
                        path = QtGui.QPainterPath(QPointF(-width_4, 0))
                        path.lineTo(width_4, 0)

                    elif slope > 0 :
                        path = QtGui.QPainterPath(QPointF(-width_4, height_4))
                        path.lineTo(0, height_4)
                        path.lineTo(width_4, -height_4)
 
                else :

                    path = QtGui.QPainterPath(QPointF(-width_4, height_4))
                    path.lineTo(0, height_4)
                    path.lineTo(width_4, -height_4)

                painter.drawPath(path) 

                return

            elif self._name == 'Step' :

                if 'start_value' in self._parameters and 'stop_value' in self._parameters :
                    
                    start_value = self._parameters['start_value']['value']
                    stop_value = self._parameters['stop_value']['value']
                    if start_value > stop_value :
                        path = QtGui.QPainterPath(QPointF(-width_4, -height_4))
                        path.lineTo(0, -height_4)
                        path.lineTo(0, height_4)
                        path.lineTo(width_4, height_4)

                    elif start_value == stop_value :
                        path = QtGui.QPainterPath(QPointF(0, -height_4))
                        path.lineTo(0, height_4)

                    else :
                        path = QtGui.QPainterPath(QPointF(-width_4, height_4))
                        path.lineTo(0, height_4)
                        path.lineTo(0, -height_4)
                        path.lineTo(width_4, -height_4)

                else :
                    path = QtGui.QPainterPath(QPointF(-width_4, height_4))
                    path.lineTo(0, height_4)
                    path.lineTo(0, -height_4)
                    path.lineTo(width_4, -height_4)

                painter.drawPath(path) 

                return

            elif self._name == 'Square_1' or self._name == 'Square_2':
                width_12 = 2 * width_4 / 7

                lengths = []
                lengths.append(-width_4)

                for i in range(0, 6) : 
                    lengths.append(lengths[i] + width_12)

                path = QtGui.QPainterPath(QPointF(lengths[0], height_4))

                if self._name == 'Square_1' :
                    
                    path.lineTo(lengths[2], height_4)
                    path.lineTo(lengths[2], -height_4)
                    path.lineTo(lengths[4], -height_4)
                    path.lineTo(lengths[4], height_4)
                    path.lineTo(lengths[6], height_4)
                    path.lineTo(lengths[6], -height_4)

                else :
                    
                    path.lineTo(lengths[1], height_4)
                    path.lineTo(lengths[2], -height_4)
                    path.lineTo(lengths[3], -height_4)
                    path.lineTo(lengths[4], height_4)
                    path.lineTo(lengths[5], height_4)
                    path.lineTo(lengths[6], -height_4)


                painter.drawPath(path) 
                return


            self.print_text(painter, option, widget)
            return
        
        super().print_box(painter, option, widget)