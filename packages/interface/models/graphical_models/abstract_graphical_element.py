#-----------------------------------
# Imports
#-----------------------------------

from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtCore import QPointF, QRectF, QSizeF, Qt, pyqtSignal
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsObject, QGraphicsSceneHoverEvent, QStyleOptionGraphicsItem

import packages.interface.models.color as c

#-----------------------------------
# Class
#-----------------------------------
        
static_white_brush : QtGui.QBrush = QtGui.QBrush(Qt.white)
static_black_brush : QtGui.QBrush = QtGui.QBrush(Qt.black)

static_unselected_black_pen : QtGui.QPen = QtGui.QPen(Qt.black)
static_unselected_black_pen.setWidth(1)

static_selected_black_pen : QtGui.QPen = QtGui.QPen(Qt.black)
static_selected_black_pen.setWidth(1)
static_selected_black_pen.setDashPattern([3, 2])

static_unselected_red_pen : QtGui.QPen = QtGui.QPen(Qt.red)
static_unselected_red_pen.setWidth(1)

static_selected_red_pen : QtGui.QPen = QtGui.QPen(Qt.red)
static_selected_red_pen.setWidth(1)
static_selected_red_pen.setDashPattern([3, 2])

class AbstractGraphicalElement (QGraphicsObject) :
    """Est la classe abstraite commune aux éléments graphiques affichés dans l'interface."""

    # -------------
    # Static Element
    # -------------

    GRAPHICAL_INDEX : int = 0
    
    # -------------
    # Signals
    # -------------

    param_changed = pyqtSignal(object, str, object, object)
    """
    Params : 
    - *object [AbstractGraphicalElement]* model
    - *str* param name
    - *object* old value
    - *object* new value
    """


    # -------------
    # Constructors
    # -------------

    def __init__ (self, name : str, position : QPointF, size : QSizeF, rotation : float = 0, text : str = '', parent : QGraphicsItem = None) :
        
        QGraphicsItem.__init__(self, parent)
        self.setParent(parent)

        self.graphical_index = AbstractGraphicalElement.GRAPHICAL_INDEX
        AbstractGraphicalElement.GRAPHICAL_INDEX += 1

        self._action_size_semaphore : bool = True 
        self._action_pos_semaphore : bool = True
        self._action_rot_semaphore : bool = True
        self._action_param_semaphore : bool = True

        self._name : str = name
        top_left : QPointF = QPointF(- size.width() / 2, - size.height() / 2)
        self._bound : QRectF = QRectF(top_left, size)
        self._text = text
        self._color = c.black

        self.setRotation(rotation)
        self.setPos(position)
        self.setAcceptHoverEvents(True)
        
    # -------------
    # Properties
    # -------------

    @property
    def graphical_index (self) -> int :
        """
        Permet de récuperer l'index graphique.
        Cet index est utilisé pour référencé les objets pour les actions.
        Il permet de retenir quel élément est quel élément peut importe l'ordre des graphical elements dans le diagram.
        Il n'est pas nécessaire de le sauvegarder dans un fichier. Les actions ne portent que sur les éléments actuellement dans l'interface.
        """
        return self._graphical_index

    @graphical_index.setter
    def graphical_index (self, graphical_index_ : int) -> None : 
        """Permet de modifier l'index graphique."""

        if graphical_index_ < 0 :
            return
        
        self._graphical_index = graphical_index_

    @property
    def white_brush (self) -> QtGui.QBrush :
        """Permet de récuperer le pinceau blanc."""
        return static_white_brush

    @property
    def black_brush (self) -> QtGui.QBrush :
        """Permet de récuperer le pinceau noir."""
        return static_black_brush

    @property
    def unselected_black_pen (self) -> QtGui.QPen :
        """Permet de récuperer le crayon noir pour les éléments non sélectionnés."""
        return static_unselected_black_pen

    @property
    def selected_black_pen (self) -> QtGui.QPen :
        """Permet de récuperer le crayon noir pour les éléments sélectionnés."""
        return static_selected_black_pen

    @property
    def unselected_red_pen (self) -> QtGui.QPen :
        """Permet de récuperer le crayon rouge pour les éléments non sélectionnés."""
        return static_unselected_red_pen

    @property
    def selected_red_pen (self) -> QtGui.QPen :
        """Permet de récuperer le crayon rouge pour les éléments sélectionnés."""
        return static_selected_red_pen

    @property
    def name (self) -> str :
        """Permet de récuperer le nom de l'élément."""
        return self._name

    @name.setter
    def name (self, name_ : str) -> None :
        """Permet de modifier le nom de l'élément."""
        self._name = name_

    @property
    def position (self) -> QPointF :
        """Permet de récuperer la position de l'élément graphique."""
        return self.pos()

    @position.setter
    def position (self, position_ : QPointF) -> None :
        """Permet de modifier la position de l'élement graphique."""
        self.setPos(position_) 

    @property
    def size (self) -> QSizeF :
        """Permet de récuperer la taille de l'élement graphique."""
        return self._bound.size()

    @size.setter
    def size (self, size_ : QSizeF) -> None :
        """Permet de modifier la taille de l'élément graphique."""
        self._bound.setSize(size_)

    @property
    def rot (self) -> float :
        """Permet de récuperer la rotation de la box."""
        return self.rotation()

    @rot.setter
    def rot (self, rot_ : float) -> None :
        """Permet de modifier la rotation de la box."""
        self.setRotation(rot_)

    @property 
    def text (self) -> str :
        """Permet de récuperer le texte à afficher."""
        return self._text

    @text.setter
    def text (self, text_ : str) -> None :
        """Permet de modifier le texte à afficher."""

        if text_ == self._text :
            return

        old_text = self._text
        self._text : str = text_ or '' 

        if self.action_param_semaphore :
            return

        self.param_changed.emit(self, 'text', old_text, self._text)
        
    @property
    def rendered_text (self) -> str :
        """Permet de récuperer le texte à afficher."""

        if self._text is None : 
            return self._name

        if self._text.strip().__len__() == 0 :
            return self._name

        return self._text

    @rendered_text.setter
    def rendered_text (self, rendered_text_ : str) -> None :
        """Permet de modifier le texte à afficher."""
        self.text = rendered_text_

    @property
    def color (self) -> c.Color :
        """Permet de récuperer la couleur de l'élément."""
        return self._color

    @color.setter
    def color (self, color_ : c.Color) -> None :
        """Permet de modifier la couleur de l'élément."""
        self._color = color_

    @property
    def diagram_model (self) :
        """Permet de récuperer l'éditeur visuel."""
        return self._diagram_model

    @diagram_model.setter
    def diagram_model (self, diagram_model_) -> None :
        """Permet de modifier l'éditeur visuel."""
        self._diagram_model = diagram_model_

        if self._diagram_model is None :
            self._action_size_semaphore : bool = True 
            self._action_pos_semaphore : bool = True
            self._action_rot_semaphore : bool = True
            self._action_param_semaphore : bool = True
        
        else : 
            self._action_size_semaphore : bool = False 
            self._action_pos_semaphore : bool = False
            self._action_rot_semaphore : bool = False
            self._action_param_semaphore : bool = False

    @property
    def action_size_semaphore (self) :
        """Permet de récuperer l'état du sémpahore pour le changement de taille de l'objet."""
        return self._action_size_semaphore

    @action_size_semaphore.setter 
    def action_size_semaphore (self, action_size_semaphore_ : bool) :
        """Permet de modifier l'état du sémaphore pour le changement de taille de l'objet."""
        self._action_size_semaphore = action_size_semaphore_

    @property
    def action_pos_semaphore (self) :
        """Permet de récuperer l'état du sémpahore pour le changement de position de l'objet."""
        return self._action_pos_semaphore

    @action_pos_semaphore.setter 
    def action_pos_semaphore (self, action_pos_semaphore_ : bool) :
        """Permet de modifier l'état du sémaphore pour le changement de position de l'objet."""
        self._action_pos_semaphore = action_pos_semaphore_

    @property
    def action_rot_semaphore (self) :
        """Permet de récuperer l'état du sémpahore pour le changement de rotation de l'objet."""
        return self._action_rot_semaphore

    @action_rot_semaphore.setter 
    def action_rot_semaphore (self, action_rot_semaphore_ : bool) :
        """Permet de modifier l'état du sémaphore pour le changement de rotation de l'objet."""
        self._action_rot_semaphore = action_rot_semaphore_

    @property
    def action_param_semaphore (self) -> bool :
        """Permet de récuperer le sémaphore pour les paramètres de l'objet."""
        return self._action_param_semaphore

    @action_param_semaphore.setter
    def action_param_semaphore (self, action_param_semaphore_ : bool) -> None :
        """Permet de modifier le sémaphore pour les paramètres de l'objet."""
        self._action_param_semaphore = action_param_semaphore_

    
    # -------------
    # Methods
    # -------------

    def paint (self, painter: QtGui.QPainter, option: QStyleOptionGraphicsItem, widget) -> None :
        """Permet d'afficher l'élément graphique dans la fenêtre."""
        super().paint(painter, option, widget)

    def boundingRect (self) -> QtCore.QRectF :
        return self._bound

    def hoverEnterEvent (self, event : QGraphicsSceneHoverEvent) -> None :
        self.update(self.boundingRect())
        return super().hoverEnterEvent(event)

    def hoverLeaveEvent (self, event : QGraphicsSceneHoverEvent) -> None :
        self.update(self.boundingRect())
        return super().hoverLeaveEvent(event)

    
