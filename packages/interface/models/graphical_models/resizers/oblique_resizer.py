#-----------------------------------
# Imports
#-----------------------------------

from PyQt5.QtCore import QPointF, QSizeF, Qt
from PyQt5.QtGui import QPainter, QPen
from packages.interface.models.graphical_models.resizers.abstract_resizer import AbstractResizer

#-----------------------------------
# Class
#-----------------------------------

class ObliqueResizer (AbstractResizer) :
    """Est le resizer qui permet de modifier la hauteur et la longueur des boxes."""

    # -------------
    # Constructers
    # -------------

    def __init__ (self, parent) :
        
        AbstractResizer.__init__(self, parent, QSizeF(10, 10))

    # -------------
    # Methods
    # -------------

    def changeValue (self, value_: QPointF) -> QPointF :
        return value_ - self.pos()