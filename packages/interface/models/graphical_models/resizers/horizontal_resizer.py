#-----------------------------------
# Imports
#-----------------------------------

from PyQt5.QtCore import QPointF, QSizeF, Qt
from PyQt5.QtGui import QPainter, QPen
from packages.interface.models.graphical_models.resizers.abstract_resizer import AbstractResizer

#-----------------------------------
# Class
#-----------------------------------

class HorizontalResizer (AbstractResizer) :
    """Est le resizer qui permet de modifier la longueur des boxes."""

    # -------------
    # Constructers
    # -------------

    def __init__ (self, parent) :
        
        AbstractResizer.__init__(self, parent, QSizeF(10, 20))

    # -------------
    # Methods
    # -------------

    def changeValue (self, value_: QPointF) -> QPointF :
        v : QPointF = value_ - self.pos()
        v.setY(0)
        return v
        