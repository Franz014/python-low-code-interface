#-----------------------------------
# Imports
#-----------------------------------

from PyQt5.QtCore import QPointF, QSizeF

from PyQt5.QtWidgets import QGraphicsItem
from packages.core.plci_core_data_type import DataType

from packages.interface.models.graphical_models.abstract_box_IO_model import AbstractBoxIOModel

from packages.interface.models.signals_model.output_connection_model import OutputConnectionModel

import packages.interface.models.color as c
from packages.interface.models.signals_model.signal_link_model import SignalLinkModel

#-----------------------------------
# Class
#-----------------------------------

class BoxInputModel (AbstractBoxIOModel) :
    """Est la classe des entrées des boxes composite."""
    
    # -------------
    # Constructors
    # -------------

    def __init__ (self, name : str, data_type : DataType, position : QPointF, size : QSizeF,
                  rotation : float = 0.0, link : SignalLinkModel = None, text : str = '',
                  color : c.Color = c.black, parent : QGraphicsItem = None) :

        AbstractBoxIOModel.__init__(self, name, data_type, position, size, rotation, text, parent)
        
        self._outputs.append(OutputConnectionModel('', data_type, QPointF(0, 0), link, None, '', color, self))
