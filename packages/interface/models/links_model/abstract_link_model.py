#-----------------------------------
# Imports
#-----------------------------------

from typing import Tuple, Optional, Any

from PyQt5 import QtGui
from PyQt5.QtGui import QPen, QColor
from PyQt5.QtCore import QPointF, QSizeF, Qt, pyqtSlot
from PyQt5.QtWidgets import QGraphicsItem, QStyleOptionGraphicsItem, QWidget

import packages.interface.models.color as c
from packages.interface.models.graphical_models.abstract_graphical_element import AbstractGraphicalElement

from packages.interface.models.links_model.link_type import LinkType

#-----------------------------------
# Function
#-----------------------------------

def get_bounds_from_points (point_1 : QPointF, point_2 : QPointF) -> Tuple :
    """Permet de récuperer le rectangle décrit par deux points."""
    
    min_x : float = 0
    max_x : float = 0
    min_y : float = 0
    max_y : float = 0

    if point_1.x() <= point_2.x() :
        min_x : float = point_1.x()
        max_x : float = point_2.x()
        
    else :
        min_x : float = point_2.x()
        max_x : float = point_1.x()


    if point_1.y() <= point_2.y() :
        min_y : float = point_1.y()
        max_y : float = point_2.y()
        
    else :
        min_y : float = point_2.y()
        max_y : float = point_1.y()

    width : float = max_x - min_x
    height : float = max_y - min_y

    return (QPointF(min_x, min_y), QSizeF(width, height))

#-----------------------------------
# Class
#-----------------------------------

path_pen = QtGui.QPen(Qt.black, 1)

class AbstractLinkModel (AbstractGraphicalElement) :
    """Est la classe abstraites aux classes représentant des liens entre les noeuds."""
    
    # -------------
    # Constructors
    # -------------

    def __init__ (self, input : Any, output : Any, link_type : LinkType, 
                  text : str = '') :
        
        self._input_model : Any = input
        self._output_model : Any = output

        self._input_position : QPointF = self._input_model.scenePos()
        self._output_position : QPointF = self._output_model.scenePos()

        self._last_input_position = self._input_position
        self._last_output_position = self._output_position

        position, size = get_bounds_from_points(self._input_position, self._output_position)

        AbstractGraphicalElement.__init__(self, 'link', position, size, 0, text, None)
        self._bound.setX(0)
        self._bound.setY(0)
        self._bound.setSize(size)

        self._link_type : LinkType = link_type
        self._link_value : float = 0.8


        self._input_model.position_changed.connect(self.input_changed)
        self._input_model.size_changed.connect(self.input_changed)
        self._input_model.rotation_changed.connect(self.input_changed)

        if not(self._input_model.parent() is None) :
            self._input_model.parent().position_changed.connect(self.input_changed)
            self._input_model.parent().size_changed.connect(self.input_changed)
            self._input_model.parent().rotation_changed.connect(self.input_changed)

        self._output_model.position_changed.connect(self.output_changed)
        self._output_model.size_changed.connect(self.output_changed)
        self._output_model.rotation_changed.connect(self.output_changed)

        if not(self._output_model.parent() is None) :
            self._output_model.parent().position_changed.connect(self.output_changed)
            self._output_model.parent().size_changed.connect(self.output_changed)
            self._output_model.parent().rotation_changed.connect(self.output_changed)

        self._about_to_erased = False

    # -------------
    # Properties
    # -------------

    @AbstractGraphicalElement.rot.setter
    def rot (self, rot_ : float) -> None :
        ...
    
    @property
    def input (self) -> Any :
        """Permet de récuperer le noeud d'entrée du lien."""
        return self._input_model

    @property 
    def output (self) -> Any :
        """Permet de récuperer le noeud de sortie du lien."""
        return self._output_model

    @property
    def link_type (self) -> LinkType :
        """Permet de récuperer le type de lien entre les deux noeuds."""
        return self._link_type

    @link_type.setter
    def link_type (self, link_type_ : LinkType) -> None :
        """Permet de modifier le type de lien entre les deux noeuds."""

        if self._link_type == link_type_ :
            return

        old_value = self._link_type

        self._link_type = link_type_

        if self._link_value > 1.0 or self._link_value < 0.0 :
            self._link_value = 0.5

        if self.action_param_semaphore :
            return

        self.param_changed.emit(self, 'link_type', old_value, self._link_type)

    @property
    def link_value (self) -> float :
        """
        Permet de récuperer la valeur du lien :
        - en cas de lien courbe : la courbure du lien.
        - en cas de lien carré : le pourcentage avant que le lien ne se brise.
        """
        return self._link_value

    @link_value.setter
    def link_value (self, link_value_ : float) -> None :
        """Permet de modifier la valeur du lien."""

        if self._link_value == link_value_ :
            return

        old_value = self._link_value

        self._link_value = link_value_

        if self._link_value > 1.0 or self._link_value < 0.0 :
            self._link_value = 0.5

        if self.action_param_semaphore :
            return

        self.param_changed.emit(self, 'link_value', old_value, self._link_value)

    @property
    def _path_pen (self) -> QtGui.QPen :
        """Permet de récuperer le pinceau pour dessiner les liens."""
        return path_pen

    # -------------
    # Methods
    # -------------

    @pyqtSlot()
    def input_changed (self) :
        """Est la méthode appelée lorsque la poistion du noeud 1 change."""
        self.self_update()

    @pyqtSlot()
    def output_changed (self) :
        """Est la méthode appelée lorsque la position du noeud 2 change."""
        self.self_update()

    def setRotation(self, rot_ : float) -> None :
        ...
    
    def paint (self, painter : QtGui.QPainter, option : QStyleOptionGraphicsItem, widget : Optional[QWidget]) -> None :
        
        if self._about_to_erased :
            return

        width = self.size.width()

        point_1 = self._input_position - self.position
        point_4 = self._output_position - self.position

        point_2 = QPointF(width * self.link_value, point_1.y())
        point_3 = QPointF(point_2.x(), point_4.y())
        
        if self.link_type == LinkType.SQUARE :
            
            path = QtGui.QPainterPath(point_1)
            path.lineTo(point_2)
            path.lineTo(point_3)
            path.lineTo(point_4)

        elif self.link_type == LinkType.CURVED : 

            point_3.setX(width * (1-self.link_value))

            path = QtGui.QPainterPath(point_1)
            path.cubicTo(point_2, point_3, point_4)

        if self.isSelected() :
            painter.setPen(QPen(QColor(0, 0, 0, 255), 3))
            painter.drawPath(path)
            painter.setPen(QPen(QColor(255, 255, 255, 255), 1))
        
        painter.drawPath(path)

    def self_update (self) -> None :
        """Est la méthode appelée lorsque la position ou la taille du node 2 est modifiée."""
        
        if self.scene() is None :
            return

        self._input_position = self._input_model.scenePos()
        self._output_position = self._output_model.scenePos()
        position, size = get_bounds_from_points(self._input_position, self._output_position)
        self.setPos(position.x(), position.y())
        self._bound.setX(0)
        self._bound.setY(0)
        self._bound.setSize(QSizeF(size.width(), size.height()))

        min_x = min(self._last_input_position.x(), self._last_output_position.x(), self._input_position.x(), self._output_position.x())
        width = max(self._last_input_position.x(), self._last_output_position.x(), self._input_position.x(), self._output_position.x()) - min_x
        
        min_y = min(self._last_input_position.y(), self._last_output_position.y(), self._input_position.y(), self._output_position.y())
        height = max(self._last_input_position.y(), self._last_output_position.y(), self._input_position.y(), self._output_position.y()) - min_y

        self.scene().update(min_x, min_y, width, height)

        self._last_input_position = self._input_position
        self._last_output_position = self._output_position

    def unbind (self) -> None :
        """Permet de détruire le lien entre les deux noeuds."""

        if self._about_to_erased :
            return

        if self in self._input_model._links :
            self._input_model.remove_link(self)
            self._input_model.update(self._input_model.boundingRect())

        if self in self._output_model._links :
            self._output_model.remove_link(self)
            self._output_model.update(self._output_model.boundingRect())
        
        self._about_to_erased = True

    def itemChange (self, change: QGraphicsItem.GraphicsItemChange, value: Any) -> Any:
        if change == QGraphicsItem.GraphicsItemChange.ItemSelectedHasChanged :
            self.self_update()

        return super().itemChange(change, value)