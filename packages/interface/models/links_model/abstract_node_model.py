#-----------------------------------
# Imports
#-----------------------------------

from typing import Generic, Iterable, Type, TypeVar, List, Any

from PyQt5 import QtGui
from PyQt5.QtCore import QPointF, QSizeF
from PyQt5.QtWidgets import QGraphicsItem, QStyleOptionGraphicsItem

from packages.interface.models.graphical_models.abstract_moveable_graphical_element import AbstractMoveableGraphicalElement
from packages.interface.models.links_model.abstract_link_model import AbstractLinkModel, path_pen
import packages.interface.models.color as c

#-----------------------------------
# Generic Type
#-----------------------------------

T = TypeVar('T', bound = AbstractLinkModel)

#-----------------------------------
# Class
#-----------------------------------

class AbstractNodeModel (Generic[T], AbstractMoveableGraphicalElement) :
    """Est la classe abstraite commune aux noeuds capables d'être affiché dans l'interface."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self, name : str, position : QPointF, size : QSizeF, links : Iterable[Type[T]],
                  text : str = '', parent : QGraphicsItem = None) :

        AbstractMoveableGraphicalElement.__init__(self, name, position, size, 0, text, parent) 

        self._links : List[Type[T]] = []

        for link in links :
            self._links.append(link)

    # -------------
    # Properties
    # -------------

    @property 
    def links (self) -> Iterable[Type[T]] :
        """Permet de récuperer la liste des liens connectés au noeud."""

        for link in self._links :
            yield link

    @property
    def _path_pen (self) -> QtGui.QPen :
        """Permet de récuperer le pinceau pour dessiner les liens."""
        return path_pen


    # -------------
    # Methods
    # -------------
 
    def __len__ (self) -> int :
        """Permet de récuperer le nombre de liens connectés au noeud."""
        return self._links.__len__()

    def setRotation (self, angle: float) -> None :
        ...

    def add_link (self, link : Any) -> None :
        """Permet d'ajouter un lien à la position données."""

        if link is None : 
            return

        self._links.append(link)

    def insert_link (self, index : int, link : Any) -> None :
        """Permet d'insérer un lien à la position données."""

        if link is None : 
            return

        if index > len(self._links) :
            return

        self._links.insert(index, link)

    def remove_link (self, link : Type[T]) -> None :
        """Permet de supprimer un lien."""

        if link is None : 
            return

        self._links.remove(link)
    
    def paint (self, painter: QtGui.QPainter, option: QStyleOptionGraphicsItem, widget) -> None:
        ...