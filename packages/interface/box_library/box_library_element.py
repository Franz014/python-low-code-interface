#-----------------------------------
# Imports
#-----------------------------------

from PyQt5 import QtGui
from PyQt5.QtCore import QMimeData, Qt
from PyQt5.QtWidgets import QLabel, QListView, QWidget
from packages.core.box_library.box_library_data import AbstractBoxData

#-----------------------------------
# Class
#-----------------------------------

class BoxLibraryElement (QWidget) :

    # -------------
    # Constructors
    # -------------

    def __init__ (self, list_parent : QListView, box_data : AbstractBoxData) :

        QWidget.__init__(self, list_parent)

        self._box_data : AbstractBoxData = box_data

        self.lbl_box_data : QLabel = QLabel(self)
        self.lbl_box_data.setText(self._box_data.box_library + '.' + self._box_data.box_name)

    # -------------
    # Properties
    # -------------

    @property
    def box_data (self) -> AbstractBoxData :
        """Permet de récuperer les données de la box à instancier."""
        return self._box_data

    # -------------
    # Methods
    # -------------
    
    def mouseMoveEvent (self, event : QtGui.QMouseEvent) -> None :
    
        mimeData : QMimeData = QMimeData()
        mimeData.setData('action', bytearray('new box'.encode()))
        mimeData.setData('box', bytearray(self._box_data._box_name.encode()))
        mimeData.setData('library', bytearray(self._box_data._box_library.encode()))
        mimeData.setData('box_data', bytearray(str(id(self._box_data)).encode()))

        drag : QtGui.QDrag = QtGui.QDrag(self)
        drag.setMimeData(mimeData)
        da : Qt.DropAction = drag.exec(Qt.CopyAction)