#-----------------------------------
# Imports
#-----------------------------------

from typing import Iterable, List
from PyQt5 import QtGui
from PyQt5.QtCore import QCoreApplication, QRect, Qt
from PyQt5.QtWidgets import QLabel, QLineEdit, QListWidget, QListWidgetItem, QWidget
from packages.core.box_library.box_library_data import AbstractBoxData
from packages.core.box_library.plci_core_boxes_libraries import BoxesLibraries
from packages.interface.box_library.box_library_element import BoxLibraryElement

#-----------------------------------
# Class
#-----------------------------------

class BoxLibrariesList (QWidget) :
    """Est l'élément graphique qui permet de maintenir les bibliothèques de boxes."""

    # -------------
    # Constructors
    # -------------

    def __init__ (self, parent : QWidget, boxes_libraries : BoxesLibraries) :
        QWidget.__init__(self, parent)

        self._boxes_libraries : BoxesLibraries = boxes_libraries

        self.init_gui()
        self.libraries_reloaded()

    # -------------
    # Properties
    # -------------

    @property
    def boxes_libraries (self) :
        """Permet de récuperer la bibliothèque de boxes."""
        return self._boxes_libraries

    # -------------
    # Methods
    # -------------

    def searched_boxes (self) -> Iterable[AbstractBoxData] : 
        """Permet de récuperer les éléments après recherche."""

        search_text = self.fld_search.text() 
        elements : List[AbstractBoxData] = []

        if search_text.__len__() == 0 :
            for box_data in self._boxes_libraries.boxes :
                elements.append(box_data)

        else :
            keywords = search_text.split(' ')
            for box_data in self._boxes_libraries.boxes :

                b = True
                for k in keywords :
                    keyword = k.strip()

                    if keyword.__len__() == 0 :
                        continue
                    
                    if not(box_data._full_box_path.__contains__(keyword)) :
                        b = False
                        break

                if b : 
                    elements.append(box_data)

        elements.sort()
        
        for el in elements :
            yield el

    def resizeEvent (self, event : QtGui.QResizeEvent) -> None :

        self.lstw_boxes.setGeometry(QRect(5, 30, self.width() - 15, self.height() - 35))
        self.fld_search.setGeometry(QRect(90, 1, self.width() - 100, 22))

        return super().resizeEvent(event)

    def libraries_reloaded (self) -> None :
        """Est la méthode appelée pour recharger la bibliothèque."""
        self.show_with_text()    
        
    def init_gui (self) -> None :
        """Est la méthode appelée pour afficher les éléments visuels dans la fenêtre."""
        
        if not self.objectName():
            self.setObjectName(u"box_libraries")
            
        self.lstw_boxes : QListWidget = QListWidget(self)
        self.lstw_boxes.setObjectName(u"lstvw_boxes")
        self.lstw_boxes.setGeometry(QRect(5, 30, self.width() - 15, self.height() - 35))

        self.fld_search : QLineEdit = QLineEdit(self)
        self.fld_search.setObjectName(u"fld_search")
        self.fld_search.setGeometry(QRect(90, 1, self.width() - 100, 22))
        self.fld_search.textChanged.connect(self.search_text_changed)

        self.lbl_search : QLabel = QLabel(self)
        self.lbl_search.setObjectName(u"lbl_search")
        self.lbl_search.setGeometry(QRect(5, 6, 76, 20))
        self.lbl_search.setAlignment(Qt.AlignLeft)

        self.retranslateUi()
        
    def retranslateUi (self) -> None :
        
        self.lbl_search.setText(QCoreApplication.translate("Recherche : ", u"Recherche : ", None))

    def search_text_changed (self) -> None :
        """Est la méthode appelée lorsque le texte contenus dans la barre de recherche est changée."""
        self.show_with_text()

    def show_with_text (self) -> None : 
        """Est la méthode appelée pour faire une recherche dans la lite des élements chargés."""

        self.lstw_boxes.clear()

        for box_data in self.searched_boxes() :
            itm : QListWidgetItem = QListWidgetItem(self.lstw_boxes)
            widget : BoxLibraryElement = BoxLibraryElement(self.lstw_boxes, box_data)
            self.lstw_boxes.addItem(itm)
            self.lstw_boxes.setItemWidget(itm, widget)
