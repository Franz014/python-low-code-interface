#-----------------------------------
# Imports
#-----------------------------------

from typing import Any

from PyQt5.QtCore import QCoreApplication, Qt, pyqtSlot
from PyQt5.QtWidgets import QLabel, QFormLayout, QComboBox, QWidget, QVBoxLayout, QHBoxLayout, QPushButton

from packages.core.plci_core_scheduler_params import SchedulerParams
from packages.core.scheduler_library.plci_core_schedulers_libraries import SchedulersLibraries
from packages.core.scheduler_library.scheduler_library_data import SchedulerData
from packages.interface.models.dbl_spin_box import DblSpinBox
from packages.interface.dialogs.abstract_dialog import AbstractDialog
from packages.interface.models.editable_models.scheduler_model import SchedulerModel
from packages.interface.models.editable_models.simulation_model import SimulationModel

#-----------------------------------
# Class
#-----------------------------------

class SimulationParametersDialog (AbstractDialog) :
    """Est la classe de dialogue qui permet de modifier les paramètres de la simulation."""
    
    # -------------
    # Constructors
    # -------------

    def __init__ (self, dialog_holder : Any, simulation_model : SimulationModel, schedulers_libraries : SchedulersLibraries) :

        AbstractDialog.__init__(self, dialog_holder, 'simulation_parameters_dialog', QCoreApplication.translate(u'dlg_simulation_parameter', u'Paramètres de simulation', None))

        self.setFixedSize(370, 160)
        self.init_ui()
        self.tranlsate_ui()

        self._simulation_model = None
        self._last_selected_index = -1
        self._schedulers_libraries = schedulers_libraries

        for scheduler_data in self._schedulers_libraries.schedulers :
            self._cmb_scheduler.addItem(scheduler_data.__str__(), scheduler_data)

        self.simulation_model = simulation_model

    # -------------
    # Properties
    # -------------

    @property 
    def simulation_model (self) -> SimulationModel :
        """Permet de récuperer le modèle de simulation."""
        return self._simulation_model

    @simulation_model.setter
    def simulation_model (self, simulation_model_ : SimulationModel) -> None :
        """Permet de modifier le modèle de simulation."""

        self._simulation_model = simulation_model_

        if self._simulation_model is None : 
            self._btn_accept.setEnabled(False)
            
            self._spn_simulation_tick.setEnabled(False)
            self._spn_simulation_tick.setValue(0)

            self._spn_simulation_time.setEnabled(False)
            self._spn_simulation_time.setValue(0)

            self._cmb_scheduler.setEnabled(False)
            if self._cmb_scheduler.count() > 0 :
                self._cmb_scheduler.setCurrentIndex(0)

        else : 
            
            self._spn_simulation_tick.setEnabled(True)
            self._spn_simulation_time.setEnabled(True)
            self._cmb_scheduler.setEnabled(True)
            
            if self._simulation_model.scheduler_model is None :
                self._spn_simulation_tick.setValue(10)
                self._spn_simulation_time.setValue(0.01)

            else : 
                if self._simulation_model.scheduler_model.params is None : 
                    self._spn_simulation_tick.setValue(10)
                    self._spn_simulation_time.setValue(0.01)
                    
                else :
                    self._spn_simulation_tick.setValue(self._simulation_model.scheduler_model.params.step_time)
                    self._spn_simulation_time.setValue(self._simulation_model.scheduler_model.params.stop_time)


            if self._cmb_scheduler.count() > 0 :

                self._cmb_scheduler.setCurrentIndex(0)

                if not(self._simulation_model.scheduler_model is None) :

                    for index in range(self._cmb_scheduler.count()) :
                        item_text = self._cmb_scheduler.itemText(index)
                        if  item_text == self._simulation_model.scheduler_model.__str__() :
                            self._last_selected_index = index
                            self._cmb_scheduler.setCurrentIndex(index)
                            break
        
        self.check_changed()

    # -------------
    # Methods
    # -------------

    @pyqtSlot()
    def button_accept_clicked (self) -> None :
        """Est la méthode appelée quand l'utilisateur clique sur le bouton accepter du dialogue."""
        
        scheduler_data : SchedulerData = self._cmb_scheduler.currentData() 
        scheduler_params = SchedulerParams(self._spn_simulation_time.value(), self._spn_simulation_tick.value())
        self._value = SchedulerModel(scheduler_data.scheduler_library, scheduler_data.scheduler_name, scheduler_params)
        self.close()

    @pyqtSlot()
    def button_cancel_clicked (self) -> None : 
        """Est la méthode appelée quand l'utilisateur clique sur le bouton annuler du dialogue."""
        self.close()

    @pyqtSlot()
    def check_changed (self) -> None : 
        """Est la méthode appelée quand l'utilisateur modifie un élément graphique. La méthode débloque alors le bouton accepter si il y a quelque chose de modifié."""

        if self.simulation_model is None or not(self._cmb_scheduler.count() > 0) :
            self._btn_accept.setEnabled(False)
            return

        if self.simulation_model.scheduler_model is None : 

            if not(self._cmb_scheduler.currentIndex() == -1) :
                self._btn_accept.setEnabled(True)
            
            else :
                self._btn_accept.setEnabled(False)

            return

        if self.simulation_model.scheduler_model.params is None : 
            self._btn_accept.setEnabled(True)
            return 

        if self._spn_simulation_tick.value() == 0 or self._spn_simulation_time.value() == 0 :
            self._btn_accept.setEnabled(False)
            return

        if self._spn_simulation_time.value()  < self._spn_simulation_tick.value() : 
            self._btn_accept.setEnabled(False)
            return


        if not(self.simulation_model.scheduler_model.params.step_time == self._spn_simulation_tick.value()) :
            self._btn_accept.setEnabled(True)
            return

        if not(self.simulation_model.scheduler_model.params.stop_time == self._spn_simulation_time.value()) :
            self._btn_accept.setEnabled(True)
            return

        
        self._btn_accept.setEnabled(False)

    def init_ui (self) -> None :
        """Est la méthode appelée pour initialiser les éléments de l'interface graphique."""

        if self.objectName() is None : 
            self.setObjectName('dlg_simulation_parameter')

        self.setLayout(QVBoxLayout())

        self._pnl_form : QWidget = QWidget()
        self._pnl_form.setLayout(QFormLayout())

        self._lbl_scheduler : QLabel = QLabel()
        self._cmb_scheduler : QComboBox = QComboBox()
        self._cmb_scheduler.currentIndexChanged.connect(self.check_changed)
        self._pnl_form.layout().addRow(self._lbl_scheduler, self._cmb_scheduler)

        self._lbl_simulation_time : QLabel = QLabel()
        self._spn_simulation_time : DblSpinBox = DblSpinBox()
        self._spn_simulation_time.setMinimum(0)
        self._spn_simulation_time.valueChanged.connect(self.check_changed)
        self._pnl_form.layout().addRow(self._lbl_simulation_time, self._spn_simulation_time)

        self._lbl_simulation_tick : QLabel = QLabel()
        self._spn_simulation_tick : DblSpinBox = DblSpinBox()
        self._spn_simulation_tick.setMinimum(0)
        self._spn_simulation_tick.valueChanged.connect(self.check_changed)
        self._pnl_form.layout().addRow(self._lbl_simulation_tick, self._spn_simulation_tick)
        self.layout().addWidget(self._pnl_form)

        self._pnl_buttons : QWidget = QWidget()
        self._pnl_buttons.setLayout(QHBoxLayout())
        self._pnl_buttons.layout().setAlignment(Qt.AlignRight)

        self._btn_accept : QPushButton = QPushButton()
        self._btn_accept.clicked.connect(self.button_accept_clicked)
        self._pnl_buttons.layout().addWidget(self._btn_accept)

        self._btn_cancel : QPushButton = QPushButton()
        self._btn_cancel.clicked.connect(self.button_cancel_clicked)
        self._pnl_buttons.layout().addWidget(self._btn_cancel)
        self.layout().addWidget(self._pnl_buttons)

    def tranlsate_ui (self) -> None : 
        """Est la méthode pour traduire les éléments de l'interface graphique."""

        self._lbl_scheduler.setText(QCoreApplication.translate(self.objectName(), u'Planificateur : ', None))
        self._lbl_simulation_time.setText(QCoreApplication.translate(self.objectName(), u'Temps de simulation : ', None))
        self._lbl_simulation_tick.setText(QCoreApplication.translate(self.objectName(), u'Pas de la simulation : ', None))
        self._btn_accept.setText(QCoreApplication.translate(self.objectName(), u'Accepter', None))
        self._btn_cancel.setText(QCoreApplication.translate(self.objectName(), u'Annuler', None))