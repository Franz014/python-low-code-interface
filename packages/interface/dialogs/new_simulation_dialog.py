#-----------------------------------
# Imports
#-----------------------------------

import os
from datetime import date
from typing import Any

from PyQt5.QtCore import QCoreApplication, QRect, pyqtSlot
from PyQt5.QtWidgets import QComboBox, QFileDialog, QFrame, QLabel, QLineEdit, QPushButton
from packages.core.plci_core_scheduler_params import SchedulerParams

from packages.core.scheduler_library.plci_core_schedulers_libraries import SchedulersLibraries
from packages.core.scheduler_library.scheduler_library_data import SchedulerData
from packages.interface.models.dbl_spin_box import DblSpinBox
from packages.interface.models.editable_models.scheduler_model import SchedulerModel
from packages.interface.models.editable_models.simulation_model import SimulationModel
from .abstract_dialog import AbstractDialog

#-----------------------------------
# Class
#-----------------------------------

class NewSimulationDialog (AbstractDialog) :
    """Est la classe permettant d'afficher une boîte de dialogue capable de créer une simulation."""
    
    # -------------
    # Constructors
    # -------------

    def __init__ (self, dialog_holder : Any, schedulers_libraries : SchedulersLibraries) :
        AbstractDialog.__init__(self, dialog_holder, 'new_simulation', 'Nouvelle Simulation')

        self.setFixedSize(480, 240)
        self._init_ui()
        self._test_data_accept_button()
        
        self._schedulers_libraries = schedulers_libraries

        for scheduler_data in self._schedulers_libraries.schedulers :
            self._cmb_scheduler.addItem(scheduler_data.__str__(), scheduler_data)


    # -------------
    # Methods
    # -------------

    @pyqtSlot()
    def _btn_cancel_click (self) -> None :
        """Est la méthode appelée lorsque l'utilisateur clique sur le bouton annuler."""
        self.close()
        
    @pyqtSlot()
    def _btn_simulation_path_click (self) -> None :
        """Est la méthode appelée lorsque l'utilisateur clique sur le bouton rechercher."""
        path = QFileDialog.getExistingDirectory(self, "Recherche d'un dossier", os.getcwd())

        if path is None or path == '':
            return

        if not path.endswith('/') :
            path = path + '/'

        self._fld_simulation_path.setText(path)

    @pyqtSlot()
    def _test_data_accept_button (self) -> None :
        """Permet de vérifier si les données entrées sont correctes dans le but de débloquer le bouton accepter."""
        
        if self._fld_simulation_name.text().strip().__len__() == 0 :
            self._btn_accept.setEnabled(False)
            return

        if not os.path.isdir(self._fld_simulation_path.text()) :
            self._btn_accept.setEnabled(False)
            return

        if self._cmb_scheduler.currentIndex() == -1 :
            self._btn_accept.setEnabled(False)
            return
        
        if self._spn_simulation_timing.value() == 0 :
            self._btn_accept.setEnabled(False)
            return

        if self._spn_simulation_step.value() == 0 :
            self._btn_accept.setEnabled(False)
            return
            
        self._btn_accept.setEnabled(True)


    def _init_ui (self) :
        self._btn_accept = QPushButton(self)
        self._btn_accept.setObjectName("_btn_accept")
        self._btn_accept.setGeometry(QRect(300, 200, 81, 24))
        self._btn_accept.clicked.connect(self._btn_accept_click)

        self._btn_cancel = QPushButton(self)
        self._btn_cancel.setObjectName("_btn_cancel")
        self._btn_cancel.setGeometry(QRect(390, 200, 81, 24))
        self._btn_cancel.clicked.connect(self._btn_cancel_click)

        self._btn_simulation_path = QPushButton(self)
        self._btn_simulation_path.setObjectName("_btn_simulation_path")
        self._btn_simulation_path.setGeometry(QRect(400, 50, 75, 24))
        self._btn_simulation_path.clicked.connect(self._btn_simulation_path_click)

        self._fld_simulation_name = QLineEdit(self)
        self._fld_simulation_name.setObjectName("_fld_simulation_name")
        self._fld_simulation_name.setGeometry(QRect(150, 20, 241, 22))
        self._fld_simulation_name.textChanged.connect(self._test_data_accept_button)
        
        self._fld_simulation_path = QLineEdit(self)
        self._fld_simulation_path.setObjectName("_fld_simulation_path")
        self._fld_simulation_path.setGeometry(QRect(150, 50, 241, 22))
        self._fld_simulation_path.textChanged.connect(self._test_data_accept_button)

        self._spn_simulation_timing = DblSpinBox(self)
        self._spn_simulation_timing.setObjectName("_spn_simulation_timing")
        self._spn_simulation_timing.setGeometry(QRect(150, 130, 241, 22))
        self._spn_simulation_timing.setMinimum(0)
        self._spn_simulation_timing.setValue(5.0)
        self._spn_simulation_timing.valueChanged.connect(self._test_data_accept_button)

        self._spn_simulation_step = DblSpinBox(self)
        self._spn_simulation_step.setObjectName("_spn_simulation_step")
        self._spn_simulation_step.setGeometry(QRect(150, 160, 241, 22))
        self._spn_simulation_step.setMinimum(0)
        self._spn_simulation_step.setValue(0.001)
        self._spn_simulation_step.setSingleStep(self._spn_simulation_step.value())
        self._spn_simulation_step.valueChanged.connect(self._test_data_accept_button)

        self._cmb_scheduler = QComboBox(self)
        self._cmb_scheduler.setObjectName("_cmb_scheduler")
        self._cmb_scheduler.setGeometry(QRect(150, 100, 241, 22))
        self._cmb_scheduler.currentIndexChanged.connect(self._test_data_accept_button)

        self._ln_separator = QFrame(self)
        self._ln_separator.setObjectName("_ln_separator")
        self._ln_separator.setGeometry(QRect(10, 80, 461, 16))
        self._ln_separator.setFrameShape(QFrame.HLine)
        self._ln_separator.setFrameShadow(QFrame.Sunken)

        self._lbl_simulation_name = QLabel(self)
        self._lbl_simulation_name.setObjectName("_lbl_simulation_name")
        self._lbl_simulation_name.setGeometry(QRect(10, 25, 165, 16))

        self._lbl_simulation_path = QLabel(self)
        self._lbl_simulation_path.setObjectName("_lbl_simulation_path")
        self._lbl_simulation_path.setGeometry(QRect(10, 55, 165, 16))

        self._lbl_scheduler = QLabel(self)
        self._lbl_scheduler.setObjectName("_lbl_scheduler")
        self._lbl_scheduler.setGeometry(QRect(10, 104, 165, 16))

        self._lbl_simulation_timing = QLabel(self)
        self._lbl_simulation_timing.setObjectName("_lbl_simulation_timing")
        self._lbl_simulation_timing.setGeometry(QRect(10, 135, 165, 16))

        self._lbl_simulation_step = QLabel(self)
        self._lbl_simulation_step.setObjectName("_lbl_simulation_step")
        self._lbl_simulation_step.setGeometry(QRect(10, 165, 131, 16))

        self.retranslateUi(self)

    def retranslateUi (self, Dialog) -> None :
        self.setWindowTitle(QCoreApplication.translate(self.objectName(), "Nouvelle Simulation", None))
        self._btn_accept.setText(QCoreApplication.translate("_dlg_new_simulation", "Accepter", None))
        self._btn_cancel.setText(QCoreApplication.translate("_dlg_new_simulation", "Annuler", None))
        self._btn_simulation_path.setText(QCoreApplication.translate("_dlg_new_simulation", "Rechercher", None))
        self._lbl_simulation_name.setText(QCoreApplication.translate("_dlg_new_simulation", "Nom de la simulation :", None))
        self._lbl_simulation_path.setText(QCoreApplication.translate("_dlg_new_simulation", "Chemin de la simulation :", None))
        self._lbl_scheduler.setText(QCoreApplication.translate("_dlg_new_simulation", "Planificateur :", None))
        self._lbl_simulation_timing.setText(QCoreApplication.translate("_dlg_new_simulation", "Temps de simulation : ", None))
        self._lbl_simulation_step.setText(QCoreApplication.translate("_dlg_new_simulation", "Pas de la simulation :", None))

    @pyqtSlot()
    def _btn_accept_click (self) -> None :
        """Est la méthode appelée lorsque l'utilisateur clique sur le bouton accepter."""

        scheduler_data : SchedulerData = self._cmb_scheduler.currentData()  
        scheduler_params : SchedulerParams = SchedulerParams(self._spn_simulation_timing.value(), self._spn_simulation_step.value())
        scheduler_model : SchedulerModel = SchedulerModel(scheduler_data.scheduler_library, scheduler_data.scheduler_name, scheduler_params)
        self._value = SimulationModel(self._fld_simulation_name.text(), self._fld_simulation_path.text(), '', date.today(), 0.1, scheduler_model, [])
        self.close()