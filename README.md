# MuPhyN - Low code simulator

Matlab + Simulink is a very powerful tool. This tool allows engineers to simulate real devices.
In open sources, some tools exist, such as Scilab + Xcos. 

With the growing community of python, some tools appeared: NumPy, SciPy, ... Those tools intend to propose a Matlab alternative environment equivalent for Python.
MuPhyN is an interface that is intended to reproduce the Simulink capabilities. The goal is to produce a community-based simulation software. 

it is based on a Qt interface and uses a simulator core. It is fully written in Python and uses YAML as the descriptive language. The library feature proposed allows users to add as many boxes and schedulers as they want. 
The already created boxes take advantage on the SciPy and NumPy libraries to produce their behaviors.

## Usage

### Drag drop

You can easily drag / drop elements from the library.
Once dropped, you can link the inputs and the outputs of the box.
You can also edit the properties of the boxes in order to change their behaviours.
Once everything is setted up, you only need to press the F5 key or click on the "Simulation > Lancer" menu.

![](https://media3.giphy.com/media/SaTScRBbeSNChgjagW/giphy.gif)

### Select library


Use the "Affichage > Bibliothèques" sub menus to add the libraries
Default libraries are located as it : 
- [MuPhyN_Folder]/box_library
- [MuPhyN_Folder]/scheduler_library
- [MuPhyN_Folder]/user_box

![](https://media4.giphy.com/media/uPfHccujlpxyGAkyF4/giphy.gif)

## Installation

Step 1 : 

Install the required libraries :
- Matplotlib
- PyQt5
- Numpy
- SciPy

Step 2 :

Download the software

Step 3 : 

Launch the software using :  "python -m main"

By the way, those folder could be placed anywhere on the drive as long as the correct link is added in the libraries dialog.
Also, more libraries could be added.

## Developpement

### Features 

#### MVP : 
- Simulation creation
- Boxes : Addition, Amplifier, Constant, Derivator, Graph, Integrator, Multiplier, Ramp, Sine, Square, Step
- Schedulers : Default
- Libraries management for both the boxes and the schedulers.
- Draggable and droppable element from the library.
- properties panel for the dragged boxes.
- Link creation between the inputs and outputs .
- Saving simulation.
- Opening back old simulation.
- Simulation parameters edition
- Simulation
- File drag / drop in the interface.
- Show recent files
- Undo / Redo
- Copy / Cut / Paste

### Todo 

- Allow to edit the prompted text on the boxes.
- Add the box composite diagram editor.
- Allow to save composite boxes.
- Allow to open composite boxes.
- Allow to compile composite boxes.
- Show better visual feedback for the link creation.
- Add a progress bar for the simulation part.
- Add a logs manager to retrace what happen when an error occur.
- Add some popups to describes the errors currently rendered in the prompt.
- Add some popups menu when right click on elements.
- Add a library popup when drag / drop a link to show compatible box.
- Allows the user to pick from the library popup a compatible box, which will be added already connected to the diagram.
- Add some popups to tell the user when an action is correctly done.
- Add a dialog to edit the parameters of the software.
- Add a dialog to show an help to use the software.
- Add a dialog to show the informations about the softawre.
- Add theme manager.
- End the formula box
- End the reccurence box
- Add a vectorial / matrix simulation mode
- Add a mux box
- Add a demux box
- Allow to pause / restart / stop a heavy simulation.
- Add a debug mode to see what happens on the different link by adding a step by step simulation.
- Add openned matplotlib chart in the dialog manager.
- Add a wizard fot the schedulers creation.
- Add a code editor for the schedulers and the coded boxes.
- Add a language dialog to manage / create the software language
- Allow to change the displayed language
- Add a theme dialog to manage / create the software themes
- Allow to change the displayed theme
- Add some icons
- Try the software on a mac os environment
- Try the software on a linux environment
- Add a signal analysis library.

### Known bugs

- When a text from an IO of a box is edited, it is not edited in the diagram.
- Some lags with the links rendering exists.
