
from typing import List

def _init_amplifier_box (box) -> None :

    if not 'gain' in box.params :
        box.params['gain'] = 1.0

def _function_amplifier_box (box, event_) -> List :
    
    v = box.params['gain'] * box.get_input(0).value
    events : List = []
    
    for output in box.outputs :
        events.append(box.construct_signal_event(output, v))

    return events