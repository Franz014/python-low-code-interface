
from typing import List

def _init_integrator_box (box) -> None :
    box.params['last_output'] = 0
    box.params['last_input'] = 0
    box.params['last_timing'] = 0


def _function_integrator_box (box, event_) -> List:
        
    input = box.get_input(0).value

    if event_.timing == 0 :
        return []

    if box.params['last_timing'] == event_.timing :
        return []

    box.params['last_timing'] = event_.timing

    v = (event_.step_time * (input + box.params['last_input']) / 2) + box.params['last_output']

    box.params['last_output'] = v
    box.params['last_input'] = input

    events : List = []
    
    for output in box.outputs :
        events.append(box.construct_signal_event(output, v))

    return events