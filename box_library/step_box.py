
from typing import List

def _init_step_box (box) -> None :

    if not 'step_time' in box.params :
        box.params['step_time'] = 1.0

    if not 'start_value' in box.params :
        box.params['start_value'] = 0.0

    if not 'stop_value' in box.params :
        box.params['stop_value'] = 1.0

def _function_step_box (box, event_) -> List : 
    if event_.timing < box.params['step_time']:
        v = box.params['start_value']
    else:
        v = box.params['stop_value']

    events : List = []
    
    for output in box.outputs :
        events.append(box.construct_signal_event(output, v))

    return events