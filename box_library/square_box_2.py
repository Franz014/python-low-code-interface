
from typing import List

def _init_square_box_2 (box) -> None :
    
    box.params['current_period'] = 0

    if not 'high_value' in box.params :
        box.params['high_value'] = 1.0

    if not 'low_value' in box.params :
        box.params['low_value'] = 0.0

    if not 'high_time' in box.params :
        box.params['high_time'] = 0.5

    if not 'low_time' in box.params :
        box.params['low_time'] = 0.5

    if not 'rise_time' in box.params: 
        box.params['rise_time'] = 0.01

    if not 'fall_time' in box.params: 
        box.params['fall_time'] = 0.01

    if box.params['fall_time'] == 0 :
        box.params['fall_slope'] = 1
    else :
        box.params['fall_slope'] = (box.params['low_value'] - box.params['high_value']) / box.params['fall_time']

    if box.params['rise_time'] == 0:
        box.params['rise_slope'] = 1
    else :
        box.params['rise_slope'] = (box.params['high_value'] - box.params['low_value'])  / box.params['rise_time']

    print("Creating square box")
    print('\t - high_time : ', box.params['high_time'])
    print('\t - low_time : ', box.params['low_time'])
    print('\t - rise_time : ', box.params['rise_time'])
    print('\t - fall_time : ', box.params['fall_time'])
    print('\t - period : ', box.params['high_time'] + box.params['low_time'])
    print('\t - rise slope : ', box.params['rise_slope'])
    print('\t - fall slope : ', box.params['fall_slope'])

    print('-- computing --')
    box.params['high_time'] = box.params['high_time'] - box.params['fall_time']
    print('\t - high_time \t= high_time - fall_time = ', box.params['high_time'])
    box.params['fall_time'] = box.params['high_time'] + box.params['fall_time'] 
    print('\t - fall_time \t= high_time + fall_time = ', box.params['fall_time'])
    box.params['low_time'] = box.params['low_time'] + box.params['fall_time'] - box.params['rise_time']
    print('\t - low_time \t= low_time + fall_time - rise_time = ', box.params['low_time'])
    box.params['rise_time'] = box.params['low_time'] + box.params['rise_time']
    print('\t - rise_time \t= low_time + rise_time = ', box.params['rise_time'])


def _function_square_box_2 (box, event_) -> List : 

    if box.params['current_period'] < box.params['high_time'] :

        v = box.params['high_value']

    elif box.params['current_period'] < box.params['fall_time']:

        v = box.params['high_value'] + (box.params['fall_slope'] * (box.params['fall_time'] - box.params['current_period']))

    elif box.params['current_period'] < box.params['low_time']:
        
        v = box.params['low_value']

    elif box.params['current_period'] < box.params['rise_time']:

        v = box.params['low_value'] + (box.params['rise_slope'] * (box.params['rise_time'] - box.params['current_period']))
    
    else :

        v = box.params['high_value']

    events : List = []
    
    for output in box.outputs :
        events.append(box.construct_signal_event(output, v))
    
    box.params['current_period'] = box.params['current_period'] + event_.step_time

    while box.params['current_period'] >= box.params['rise_time'] :
        box.params['current_period'] = box.params['current_period'] - box.params['rise_time']

    return events