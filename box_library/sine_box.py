
from typing import List
from numpy import sin

def _init_sine_box (box) -> None :

    if not 'amplitude' in box.params :
        box.params['amplitude'] = 1.0

    if not 'pulsation' in box.params :
        box.params['pulsation'] = 1.0
    
    if not 'phase' in box.params :
        box.params['phase'] = 0.0

def _function_sine_box (box, event_) -> List : 
    
    v = box.params['amplitude'] * sin((event_.timing * box.params['pulsation']) + box.params['phase'] )
    
    events : List = []

    for output in box.outputs :
        events.append(box.construct_signal_event(output, v))

    return events