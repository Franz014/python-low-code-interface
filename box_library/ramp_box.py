
from typing import List

def _init_ramp_box (box) -> None :

    if not 'start_time' in box.params :
        box.params['start_time'] = 0.0

    if not 'initial_value' in box.params :
        box.params['initial_value'] = 0.0

    if not 'slope' in box.params :
        box.params['slope'] = 1.0

def _function_ramp_box (box, event_) -> List : 
    if event_.timing < box.params['start_time']:
        v = box.params['initial_value']
    else:
        v = box.params['initial_value'] + ((event_.timing - box.params['start_time']) * box.params['slope'])

    events : List = []
    
    for output in box.outputs :
        events.append(box.construct_signal_event(output, v))

    return events