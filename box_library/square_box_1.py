
from typing import List
import math

def _init_square_box_1 (box) -> None :
    
    box.params['current_period'] = 0
    box.params['period_max'] = (math.pi * 2) / box.params['pulsation']
    box.params['switch_period'] = box.params['period_max'] * box.params['duty_cycle']
    box.params['high_value'] = box.params['mean_value'] + (box.params['amplitude'] / 2)
    box.params['low_value'] = box.params['mean_value'] - (box.params['amplitude'] / 2)


def _function_square_box_1 (box, event_) -> List : 

    if event_.timing == 0 :
        box.params['current_period'] = 0
    else :
        box.params['current_period'] = box.params['current_period'] + event_.step_time

    while box.params['current_period'] >= box.params['period_max'] :
        box.params['current_period'] = box.params['current_period'] - box.params['period_max']
        
    if box.params['current_period'] < box.params['switch_period'] :
        v = box.params['low_value']

    elif box.params['current_period'] >= box.params['switch_period'] :
        v = box.params['high_value']

    events : List = []
    
    for output in box.outputs :
        events.append(box.construct_signal_event(output, v))

    return events