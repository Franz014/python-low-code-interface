
from typing import List

def _init_reccurence_box (box) :

    box.params['values'] = []
    box.params['points_count'] = 0

    if len(box.inputs) == 0 :
        box._wait_for_all_signal_events = False
        box._wait_for_events = False
    
    box.params['values'].append([])

    for x in box.inputs :
        box.params['values'].append([])

def _function_reccurence_box (box, event_) -> List:
    v = 0

    # Pour chaque entrée ajouter la valeur actuelle dans la liste.
    i = 1
    for input in box.inputs:
        box.params['values'][i].insert(0, input.value)
        i += 1
    
    box.params['points_count'] = box.params['points_count'] + 1

    # Récuperation de la valeur indépenndante
    if len(box.params['coefficients']) > 0:
        if len(box.params['coefficients'][0]) > 0 :
            v = box.params['coefficients'][0][0]
    
    # Anciennes valeurs de sorties
    for j in range(min(len(box.params['coefficients'][0])-1, len(box.params['values'][0]))):
        v += box.params['coefficients'][0][j + 1] * box.params['values'][0][j]

    # Anciennes valeurs des entrées
    for input_index in range(min((len(box.params['values']) - 1), (len(box.params['coefficients']) - 1))) :
        values_index_count = min((len(box.params['coefficients'][input_index + 1])), (len(box.params['values'][input_index + 1])))

        for value_index in range(values_index_count) :
            v += (box.params['coefficients'][input_index + 1][value_index]) * (box.params['values'][input_index + 1][value_index]) 
    
    # Ajout de la valeur aux anciennes valeurs.
    box.params['values'][0].insert(0, v)
    
    events : List = []

    for output in box.outputs :
        events.append(box.construct_signal_event(output, v))

    return events