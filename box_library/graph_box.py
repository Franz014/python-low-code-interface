
import matplotlib.pylab as plt
from typing import List

def _init_graph_box (box) -> None :
    box.params['data_x'] = []
    box.params['data_y'] = []
    for input in box.inputs :
        box.params['data_y'].append([])

    box.params['point_count'] = 0

    if not 'title' in box.params :
        box.params['title'] = str(box.name) + " " + str(box.index)
    
    if not 'label_x' in box.params :
        box.params['label_x'] = ' '

    if not 'label_y' in box.params :
        box.params['label_y'] = ' '

    if not 'start_time' in box.params :
        box.params['start_time'] = 1.0

    if not 'stop_time' in box.params : 
        box.params['stop_time'] = 100.0

def _function_graph_box (box, event_) -> List:

    if box.params['copy_timing_from_simulation'] == True or (event_.timing > box.params['start_time'] and event_.timing < box.params['stop_time']) :
        box.params['data_x'].append(event_.timing)
        
        current_input_index = 0
        for input in box.inputs :
            box.params['data_y'][current_input_index].append(input.value)
            current_input_index += 1

        box.params['point_count'] = box.params['point_count'] + 1

    return []

def _end_graph_box (box) -> None : 

    fig = plt.figure(str(box.index) + '-' +  str(box.params['title'])) 
    plt.grid(True)
    plt.title(box.params['title'])
    plt.xlabel(box.params['label_x'])
    plt.ylabel(box.params['label_y'])

    for column in box.params['data_y'] :
        plt.plot(box.params['data_x'], column)