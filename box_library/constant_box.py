
from typing import List

def _init_constant_box (box) -> None :
    
    if not 'value' in box.params :
        box.params['value'] = 1

def _function_constant_box (box, event_) -> List :
    
    v = box.params['value']
    events : List = []
    
    for output in box.outputs :
        events.append(box.construct_signal_event(output, v))

    return events