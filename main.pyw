#-----------------------------------
# Imports
#-----------------------------------

import sys
import ctypes
import os

from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSize
from PyQt5.QtWidgets import QApplication

from packages.interface.main_window import MainWindow 

#-----------------------------------
# Main method
#-----------------------------------

if __name__ == '__main__':

    myappid = 'ceref.muphyn.1.2'

    if os.name == 'nt' :
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

    app = QApplication(sys.argv)

    app_icon = QIcon()
    app_icon.addFile('./packages/interface/assets/MuPhyN.ico', QSize(400, 400))
    app.setWindowIcon(app_icon)

    win = MainWindow()
    win.show()
    
    sys.exit(app.exec_())
